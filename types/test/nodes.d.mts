/**
 * @param {string} key
 * @param {string[]} values
 * @returns {import('../src/nodes.mjs').TagNodeProp}
 */
export function htmlattr(key: string, ...values: string[]): import('../src/nodes.mjs').TagNodeProp;
/**
 * @param {string} key
 * @param {string[]} values
 * @returns {import('../src/nodes.mjs').TagNodeProp}
 */
export function cssattr(key: string, ...values: string[]): import('../src/nodes.mjs').TagNodeProp;
