/**
 * @param {string} markup
 */
export function expectValidHTML(markup: string): void;
