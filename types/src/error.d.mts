/**
 * @param {string} data
 * @param {number} pos
 * @returns {ExtractedLine}
 */
export function extractLineAtPos(data: string, pos: number, offset_line_number?: number): ExtractedLine;
/**
 * @param {string} data
 * @param {number} pos
 * @returns {ExtractedLines}
 */
export function extractLines(data: string, pos: number, offset_main_line?: number): ExtractedLines;
/**
 * @param {string} file
 * @param {string} data
 * @param {number} pos
 * @param {string} message
 * @param {string} message_human
 * @returns {string}
 */
export function formatLineWithError(file: string, data: string, pos: number, message: string, message_human: string, offset_main_line?: number): string;
export type NoxHTMLErrorType = string;
export namespace NoxHTMLErrorType {
    const Tokenizer: string;
    const AST: string;
    const toHTML: string;
}
/** @extends Error */
export class NoxHTMLError extends Error {
    /**
     * @param {NoxHTMLError['type']} type
     * @param {NoxHTMLError['message']} message
     * @param {NoxHTMLError['position']} position
     * @param {NoxHTMLError['should_beautify_stack']} [beautify_stack=true]
     */
    constructor(type: NoxHTMLError['type'], message: NoxHTMLError['message'], position: NoxHTMLError['position'], beautify_stack?: boolean | undefined);
    /** @type {number} */
    position: number;
    /** @type {string} */
    type: string;
    /** @type {boolean} */
    should_beautify_stack: boolean;
    /**
     * @param {any} constructor_opt
     * @param {number} call_stack_offset
     * @param {TemplateStringsArray} strings
     * @param {any[]} values
     * @returns {void}
     */
    beautify(constructor_opt: any, call_stack_offset: number, strings: TemplateStringsArray, ...values: any[]): void;
}
export type ExtractedLines = {
    before: ExtractedLine | null;
    main: ExtractedLine;
    after: ExtractedLine | null;
};
export type ExtractLines = {
    line: number;
    column: number;
    lineText: string;
    lineBeforeText: string | null;
    lineAfterText: string | null;
};
export type ExtractedLine = {
    line_number: number;
    column: number;
    start_pos: number;
    end_pos: number;
    text: string;
};
