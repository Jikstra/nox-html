/** @typedef {import("./nodes.mjs").RootNode} RootNode */
/** @typedef {RootNode|null} AST  */
/**
 * @param {import("./tokenizer.mjs").Token[]} tokens
 * @returns {AST}
 */
export function toAST(tokens: import("./tokenizer.mjs").Token[]): AST;
/**
 * @param {ASTState} state
 * @param {string} branch
 * @returns {void}
 */
export function logStateAndBranch(state: ASTState, branch: string): void;
/** @param {ASTState} state
 * @param {Collected} collected
 * @param {import("./tokenizer.mjs").Token} token
 * @returns {void}
 */
export function logState(state: ASTState, collected: Collected, token: import("./tokenizer.mjs").Token): void;
/**
 * @param {Tree} tree
 * @returns {void}
 */
export function logTree(tree: Tree): void;
/** @param {Tree} tree
 * @param {Collected} collected
 * @returns {any}
 */
export function insertCollectedTagNodeAndReset(tree: Tree, collected: Collected): any;
/** @param {import("./tokenizer.mjs").TemplateValueToken['value']} template_value
 * @returns {any}
 */
export function templateValueToNode(template_value: import("./tokenizer.mjs").TemplateValueToken['value']): any;
/**
 * @param {import("./tokenizer.mjs").Token} token
 * @param {ASTState} state
 * @param {(newState: ASTState) => void} setState
 * @param {Collected} collected
 * @param {Tree} tree
 * @returns {void}
 */
export function processToken(token: import("./tokenizer.mjs").Token, state: ASTState, setState: (newState: ASTState) => void, collected: Collected, tree: Tree): void;
/** @extends NoxHTMLError */
export class NoxHTMLASTError extends NoxHTMLError {
    constructor(message: string, position: number);
}
/** */
export class Collected {
    /** @type {string} */
    tag: string;
    /** @type {'class' | 'id' | ''} */
    class_id_tag: 'class' | 'id' | '';
    /** @type {string} */
    current_identifier: string;
    /** @type {'html'|'css'} */
    current_identifier_type: 'html' | 'css';
    /** @type {TagNode['props']} */
    props: TagNode['props'];
    /** @type {null|function} */
    mixin: null | Function;
    /** @type {boolean} */
    isChained: boolean;
    /** @returns {void} */
    reset(): void;
    /**
     * @param {import("./tokenizer.mjs").TemplateValueToken['value']} value
     * @returns {string[] | null}
     */
    stringifyValue(value: import("./tokenizer.mjs").TemplateValueToken['value']): string[] | null;
    /**
     * @param {string} key
     * @param {import("./tokenizer.mjs").TemplateValueToken['value']} value
     * @returns {void}
     */
    addProp(key: string, value: import("./tokenizer.mjs").TemplateValueToken['value']): void;
    /** @returns {any} */
    toTagNode(): any;
    /** @returns {string} */
    toString(): string;
}
/** */
export class ASTify {
    /**
     * @param {import("./tokenizer.mjs").Token[]} tokens
     */
    constructor(tokens: import("./tokenizer.mjs").Token[]);
    /** @type {Tree} */
    tree: Tree;
    /** @type {Collected} */
    collected: Collected;
    /** @type {ASTState} */
    state: ASTState;
    /** @type {import("./tokenizer.mjs").Token[]} */
    tokens: import("./tokenizer.mjs").Token[];
    /** @type {number} */
    i: number;
    /** @returns {void} */
    next(): void;
    /** @returns {boolean} */
    done(): boolean;
    /** @param {ASTState} newState
     * @returns {void}
     */
    setState(newState: ASTState): void;
}
export type RootNode = import("./nodes.mjs").RootNode;
export type AST = RootNode | null;
type ASTState = string;
declare namespace ASTState {
    const INIT: string;
    const TAG: string;
    const CLASS_ID_TAG_OPERATOR: string;
    const TAG_SEPERATOR_OPEN: string;
    const TAG_PROPS_IDENTIFIER: string;
    const TAG_PROPS_EQUALS: string;
}
import { Tree } from "./tree.mjs";
import { NoxHTMLError } from "./error.mjs";
import { TagNode } from "./nodes.mjs";
export {};
