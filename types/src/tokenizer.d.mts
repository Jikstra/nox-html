/**
 * @param {string} char
 * @param {number} pos
 * @returns {TemplateChar}
 */
export function createTemplateChar(char: string, pos: number): TemplateChar;
/**
 * @param {any} value
 * @param {number} pos
 * @returns {TemplateValue}
 */
export function createTemplateValue(value: any, pos: number): TemplateValue;
/**
 * @param {TemplateStringsArray} strings
 * @param {any[]} values
 * @returns {Generator<TemplateChar | TemplateValue, void, unknown>}
 */
export function TemplateIterator(strings: TemplateStringsArray, ...values: any[]): Generator<TemplateChar | TemplateValue, void, unknown>;
/**
 * @param {string} tokenName
 * @param {number} position
 * @param {{[key: string]: string}} props
 * @returns {string}
 */
export function tokenToString(tokenName: string, position: number, props: {
    [key: string]: string;
}): string;
/**
 * @param {TemplateStringsArray} strings
 * @param {any[]} values
 * @returns {Token[]}
 */
export function Tokenizer(strings: TemplateStringsArray, ...values: any[]): Token[];
/** @implements BaseToken */
export class IdentifierToken implements BaseToken {
    /**
     * @param {IdentifierToken['identifier']} identifier
     * @param {IdentifierToken['position']} position
     */
    constructor(identifier: IdentifierToken['identifier'], position: IdentifierToken['position']);
    /** @type {'identifier'} */
    type: 'identifier';
    /** @type {number} */
    position: number;
    /** @type {string} */
    identifier: string;
    /** @returns {string} */
    toString(): string;
}
/** @implements BaseToken */
export class SeperatorToken implements BaseToken {
    /**
     * @param {SeperatorToken['seperator']} seperator
     * @param {SeperatorToken['position']} position
     */
    constructor(seperator: SeperatorToken['seperator'], position: SeperatorToken['position']);
    /** @type {'seperator'} */
    type: 'seperator';
    /** @type {BaseToken['position']} */
    position: BaseToken['position'];
    /** @type {string} */
    seperator: string;
    /** @returns {string} */
    toString(): string;
}
/** @implements BaseToken */
export class OperatorToken implements BaseToken {
    /**
     * @param {OperatorToken['operator']} operator
     * @param {OperatorToken['position']} position
     */
    constructor(operator: OperatorToken['operator'], position: OperatorToken['position']);
    /** @type {'operator'} */
    type: 'operator';
    /** @type {BaseToken['position']} */
    position: BaseToken['position'];
    /** @type {string} */
    operator: string;
    /** @returns {string} */
    toString(): string;
}
/** @implements BaseToken */
export class LiteralToken implements BaseToken {
    /**
     * @param {LiteralToken['literal']} literal
     * @param {LiteralToken['position']} position
     */
    constructor(literal: LiteralToken['literal'], position: LiteralToken['position']);
    /** @type {'literal'} */
    type: 'literal';
    /** @type {BaseToken['position']} */
    position: BaseToken['position'];
    /** @type {string}  */
    literal: string;
    /** @returns {string} */
    toString(): string;
}
/** @implements BaseToken */
export class TemplateValueToken implements BaseToken {
    /**
     * @param {TemplateValueToken['value']} value
     * @param {TemplateValueToken['position']} position
     */
    constructor(value: TemplateValueToken['value'], position: TemplateValueToken['position']);
    /** @type {'template_value'} */
    type: 'template_value';
    /** @type {BaseToken['position']} */
    position: BaseToken['position'];
    /** @type {string | Function | {[key: string]: boolean|null|undefined} | Array<string| boolean| null| undefined> | null | undefined} */
    value: string | Function | {
        [key: string]: boolean | null | undefined;
    } | (string | boolean | null | undefined)[] | null | undefined;
    /** @returns {string} */
    toString(): string;
}
/** @extends NoxHTMLError */
export class NoxHTMLTokenizerError extends NoxHTMLError {
    /**
     * @param {NoxHTMLError['message']} message
     * @param {NoxHTMLTokenizerError['message_human']} message_human
     * @param {NoxHTMLError['position']} position
     */
    constructor(message: NoxHTMLError['message'], message_human: NoxHTMLTokenizerError['message_human'], position: NoxHTMLError['position']);
    /** @type {string} */
    message_human: string;
}
export type TokenIteratorStates = string;
export namespace TokenIteratorStates {
    const INIT: string;
    const COULD_BE_IDENTIFIER: string;
    const COULD_BE_LITERAL: string;
    const COULD_BE_PIPE_LITERAL: string;
    const COULD_BE_PIPE_LITERAL_CHARS: string;
    const COULD_BE_PIPE_LITERAL_NEWLINE: string;
}
export type TemplateChar = {
    type: 'char';
    char: string;
    pos: number;
};
export type TemplateValue = {
    type: 'value';
    value: any;
    pos: number;
};
export type TemplateElement = TemplateValue | TemplateChar;
export type Token = IdentifierToken | SeperatorToken | OperatorToken | LiteralToken | TemplateValueToken;
export type LineColumn = [number, number];
export type TokenizerCollected = {
    collected: string;
    collected_position: number;
    encode: boolean;
};
export type BaseToken = {
    type: string;
    position: number;
};
import { NoxHTMLError } from "./error.mjs";
