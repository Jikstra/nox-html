/** */
export class Tree {
    /** @type {RootNode | null} */
    ast: RootNode | null;
    /** @type {RootNode | null} */
    currentNode: RootNode | null;
    /** @type {ASTNode | null}  */
    lastInsertedNode: ASTNode | null;
    /** @returns {void} */
    climbDown(): void;
    /** @param {ASTNode} node
     * @returns {void}
     */
    climbToNode(node: ASTNode): void;
    /** @param {ASTNode} node
     * @returns {any}
     */
    insertNode(node: ASTNode): any;
}
export type ASTNode = import("./nodes.mjs").ASTNode;
import { RootNode } from "./nodes.mjs";
