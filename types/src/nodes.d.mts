/**
 * @param {boolean} truncateChildren
 * @param {number} depth
 * @param {string} name
 * @param {any} args
 * @param {ASTNode[]} children
 * @returns {string}
 */
export function nodeFormatter(truncateChildren: boolean, depth: number, name: string, args: any, children: ASTNode[]): string;
/** @param {ASTNode[]} children
 * @param {ASTNode} parentNode
 * @returns {any[]}
 */
export function mapChildrenToParentNode(children: ASTNode[], parentNode: ASTNode): any[];
/** @interface */
export class ASTNode {
    /** @type {string} */
    type: string;
    /** @type {ASTNode[]} */
    children: ASTNode[];
    /** @type {ASTNode | null} */
    parentNode: ASTNode | null;
    /** @type {boolean} */
    isChained: boolean;
    /** @returns {any} */
    clone(): any;
    /**
     * @param {boolean} [truncateChildren]
     * @param {number} [depth]
     * @returns {string}
     */
    toString(truncateChildren?: boolean | undefined, depth?: number | undefined): string;
}
export class ChainableNode {
    /**
     * @param {boolean} [isChained=true]
     * @returns {ASTNode & ChainableNode}
     */
    chain(isChained?: boolean | undefined): ASTNode & ChainableNode;
}
/** @implements ASTNode */
export class RootNode implements ASTNode {
    /**
     * @param {RootNode['children']} children
     */
    constructor(children: RootNode['children']);
    /** @type {ASTNode['type']} */
    type: ASTNode['type'];
    /** @type {ASTNode['children']} */
    children: ASTNode['children'];
    /** @type {ASTNode['parentNode']} */
    parentNode: ASTNode['parentNode'];
    /** @type {ASTNode['isChained']} */
    isChained: ASTNode['isChained'];
    /** @returns {RootNode} */
    clone(): RootNode;
    /** @returns {string} */
    toString(truncateChildren?: boolean, depth?: number): string;
}
/** @typedef {null|function} Mixin */
/**
 * @implements ChainableNode
 * @implements ASTNode
 */
export class TagNode implements ChainableNode, ASTNode {
    /**
     * @param {TagNode['tag']} tag
     * @param {TagNode['props']} props
     * @param {ASTNode['parentNode']} parentNode
     * @param {ASTNode['children']} children
     */
    constructor(tag: TagNode['tag'], props: TagNode['props'], parentNode: ASTNode['parentNode'], children?: ASTNode['children']);
    /** @type {ASTNode['type']} */
    type: ASTNode['type'];
    /** @type {ASTNode['children']} */
    children: ASTNode['children'];
    /** @type {ASTNode['parentNode']} */
    parentNode: ASTNode['parentNode'];
    /** @type {ASTNode['isChained']} */
    isChained: ASTNode['isChained'];
    /** @type {string} */
    tag: string;
    /** @type {Mixin} */
    mixin: Mixin;
    /** @type {TagNodeProp[]} */
    props: TagNodeProp[];
    /**
     * @returns {TagNode}
     */
    clone(): TagNode;
    /**
     * @returns {string}
     */
    toString(truncateChildren?: boolean, depth?: number): string;
    /**
     * @param {boolean} [isChained=true]
     * @returns {TagNode}
     */
    chain(isChained?: boolean | undefined): TagNode;
    /**
     * @param {TagNode['mixin']} mixin
     * @returns {TagNode}
     */
    set_mixin(mixin: TagNode['mixin']): TagNode;
}
/**
 * @implements ChainableNode
 * @implements ASTNode
 */
export class LiteralNode implements ChainableNode, ASTNode {
    /**
     * @param {LiteralNode['content']} content
     * @param {LiteralNode['encode']} encode
     * @param {LiteralNode['parentNode']} parentNode
     * @param {LiteralNode['children']} children
     */
    constructor(content: LiteralNode['content'], encode: LiteralNode['encode'], parentNode: LiteralNode['parentNode'], children?: LiteralNode['children']);
    /** @type {ASTNode['type']} */
    type: ASTNode['type'];
    /** @type {ASTNode['children']} */
    children: ASTNode['children'];
    /** @type {ASTNode['parentNode']} */
    parentNode: ASTNode['parentNode'];
    /** @type {ASTNode['isChained']} */
    isChained: ASTNode['isChained'];
    /** @type {string} content */
    content: string;
    /** @type {boolean} */
    encode: boolean;
    /** @returns {LiteralNode} */
    clone(): LiteralNode;
    /** @returns {string} */
    toString(truncateChildren?: boolean, depth?: number): string;
    /**
     * @param {boolean} [isChained=true]
     * @returns {LiteralNode}
     */
    chain(isChained?: boolean | undefined): LiteralNode;
}
export type Mixin = null | Function;
export type TagNodeProp = {
    type: 'html' | 'css';
    key: string;
    values: string[];
};
