/**
 * @param {TemplateStringsArray} strings
 * @param {any[]} values
 * @returns {any}
 */
export function html(strings: TemplateStringsArray, ...values: any[]): any;
export class NoxHTML {
    constructor(strings: TemplateStringsArray, ...values: any[]);
    /** @type {import('./ast.mjs').AST} */
    ast: import('./ast.mjs').AST;
    /** @type {import('./tokenizer.mjs').Token[]} */
    tokens: import('./tokenizer.mjs').Token[];
    toString(): string;
}
