/** @returns {{ file: string; line: number; column: number; }} */
export function getLineAndFileOfCaller(): {
    file: string;
    line: number;
    column: number;
};
/** @param {any[]} arr
 * @returns {any}
 */
export function arrayLast(arr: any[]): any;
/**
 * @template D
 * @param {D} toClone
 * @returns {D}
 */
export function structuredClone<D>(toClone: D): D;
