/**
 * @param {ASTNode} node
 * @returns {string}
 */
export function nodeToHTML(node: ASTNode): string;
/** @param {string} tagName
 * @returns {boolean}
 */
export function isVoidElement(tagName: string): boolean;
/** @param {TagNode} node
 * @returns {string}
 */
export function tagNodeToHTML(node: TagNode): string;
/** @param {LiteralNode} node
 * @returns {string}
 */
export function literalNodeToHTML(node: LiteralNode): string;
/**
 * @param {AST} ast
 * @returns {string}
 */
export function AST2HTML(ast: AST): string;
export function camelToKebabCase(str: string): string;
export type AST = import("./ast.mjs").AST;
export type TagNode = import("./nodes.mjs").TagNode;
export type LiteralNode = import("./nodes.mjs").LiteralNode;
export type ASTNode = import("./nodes.mjs").ASTNode;
