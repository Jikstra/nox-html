import { expect } from 'chai'
import { html } from '../src/index.mjs'

import {
  Tokenizer,
  TemplateIterator,
  createTemplateChar,
  createTemplateValue,
  IdentifierToken,
  SeperatorToken,
  OperatorToken,
  LiteralToken,
  TemplateValueToken,
  NewlineToken,
} from '../src/tokenizer.mjs'

/** @typedef {import("../src/tokenizer.mjs").Token} Token */

describe('TemplateIterator', () => {
  it('Should iterate over chars in strings and inserted values', () => {
    const templateValue = 'test'
    const templateIterator = TemplateIterator`h1${templateValue}`
    expect(templateIterator.next().value).to.deep.equal(
      createTemplateChar('h', 0)
    )
    expect(templateIterator.next().value).to.deep.equal(
      createTemplateChar('1', 1)
    )
    expect(templateIterator.next().value).to.deep.equal(
      createTemplateValue(templateValue, 2)
    )
  })
  it('Should iterate over chars in strings and multiple inserted values', () => {
    const templateValue = 'test'
    const templateIterator = TemplateIterator`h1${templateValue} ${templateValue}`
    expect(templateIterator.next().value).to.deep.equal(
      createTemplateChar('h', 0)
    )
    expect(templateIterator.next().value).to.deep.equal(
      createTemplateChar('1', 1)
    )
    expect(templateIterator.next().value).to.deep.equal(
      createTemplateValue(templateValue, 2)
    )
    expect(templateIterator.next().value).to.deep.equal(
      createTemplateChar(' ', 3)
    )
    expect(templateIterator.next().value).to.deep.equal(
      createTemplateValue(templateValue, 4)
    )
  })
})

describe('Tokenizer', () => {
  it('Should tokenize a simple expression', () => {
    /** @type {Token[]} result */
    let result = Tokenizer`a(`
    expect(result).to.deep.equal([
      new IdentifierToken('a', 0),
      new SeperatorToken('(', 1),
    ])
  })
  it('Should tokenize another simple expression', () => {
    /** @type {Token[]} result */
    let result = Tokenizer`a=b`
    expect(result).to.deep.equal([
      new IdentifierToken('a', 0),
      new OperatorToken('=', 1),
      new IdentifierToken('b', 2),
    ])
  })
  it('Should tokenize a literal', () => {
    /** @type {Token[]} result */
    let result = Tokenizer`a='b'`
    expect(result).to.deep.equal([
      new IdentifierToken('a', 0),
      new OperatorToken('=', 1),
      new LiteralToken('b', 2),
    ])
  })
  it('Should tokenize a tag with arguments', () => {
    /** @type {Token[]} result */
    let result = Tokenizer`a(foo="a" bar="b")`
    expect(result).to.deep.equal([
      new IdentifierToken('a', 0),
      new SeperatorToken('(', 1),
      new IdentifierToken('foo', 2),
      new OperatorToken('=', 5),
      new LiteralToken('a', 6),
      new IdentifierToken('bar', 10),
      new OperatorToken('=', 13),
      new LiteralToken('b', 14),
      new SeperatorToken(')', 17),
    ])
  })
  it('Should tokenize a multiline tag with arguments', () => {
    /** @type {Token[]} result */
    let result = Tokenizer`a(
 foo="a"
 bar="b"
)`
    expect(result).to.deep.equal([
      new IdentifierToken('a', 0),
      new SeperatorToken('(', 1),
      new NewlineToken(2),
      new IdentifierToken('foo', 4),
      new OperatorToken('=', 7),
      new LiteralToken('a', 8),
      new NewlineToken(11),
      new IdentifierToken('bar', 13),
      new OperatorToken('=', 16),
      new LiteralToken('b', 17),
      new NewlineToken(20),
      new SeperatorToken(')', 21),
    ])
  })
  it('Should tokenize simple tag starting and ending with newline', () => {
    let tokens = Tokenizer`
  p(class="test") 
`
    expect(tokens).to.deep.equal([
      new NewlineToken(0),
      new IdentifierToken('p', 3),
      new SeperatorToken('(', 4),
      new IdentifierToken('class', 5),
      new OperatorToken('=', 10),
      new LiteralToken('test', 11),
      new SeperatorToken(')', 17),
      new NewlineToken(19),
    ])
  })
  it('Should tokenize simple tag without parameters', () => {
    let tokens = Tokenizer`p`
    expect(tokens).to.deep.equal([new IdentifierToken('p', 0)])
  })
  it('Should tokenize simple tag without parameters followed by curly brackets', () => {
    let tokens = Tokenizer`p { }`
    expect(tokens).to.deep.equal([
      new IdentifierToken('p', 0),
      new SeperatorToken('{', 2),
      new SeperatorToken('}', 4),
    ])
  })
  it('Should tokenize classtag', () => {
    let tokens = Tokenizer`.test`
    expect(tokens).to.deep.equal([
      new OperatorToken('.', 0),
      new IdentifierToken('test', 1),
    ])
  })
  it('Should tokenize idtag', () => {
    let tokens = Tokenizer`#test`
    expect(tokens).to.deep.equal([
      new OperatorToken('#', 0),
      new IdentifierToken('test', 1),
    ])
  })
  it('Should tokenize tag with id operator', () => {
    let tokens = Tokenizer`p#test`
    expect(tokens).to.deep.equal([
      new IdentifierToken('p', 0),
      new OperatorToken('#', 1),
      new IdentifierToken('test', 2),
    ])
  })
  it('Should tokeinze identifiers with numbers', () => {
    let tokens = Tokenizer`h1`
    expect(tokens).to.deep.equal([new IdentifierToken('h1', 0)])
  })

  it('Should tokenize template values', () => {
    let template_value = 'test'
    let tokens = Tokenizer`h1${template_value} ${template_value}`
    expect(tokens).to.deep.equal([
      new IdentifierToken('h1test', 0),
      new TemplateValueToken(template_value, 4),
    ])
  })

  it('Should tokenize piped text', () => {
    let tokens = Tokenizer`
                            |Foo
                            |bar`
    expect(tokens).to.deep.equal([
      new NewlineToken(0),
      new LiteralToken('Foo\nbar', 29)
    ])
  })
  
  it('Should tokenize another piped text', () => {
    let tokens = Tokenizer`
  p:
    |This is Line 1
    |This is Line 2
    |
    |This is Line 4
`
    expect(tokens).to.deep.equal([
      new NewlineToken(0),
      new IdentifierToken('p', 3),
      new OperatorToken(':', 4),
      new NewlineToken(5),
      new LiteralToken('This is Line 1\nThis is Line 2\n\nThis is Line 4\n', 10)
    ])
  })
  
  it('Should tokenize piped text with code example', () => {
    let tokens = Tokenizer`
                            |fn foobar(test) {
                            |    console.log(test)
                            |    return test;
                            |}`
    expect(tokens).to.deep.equal([
      new NewlineToken(0),
      new LiteralToken(`fn foobar(test) {
    console.log(test)
    return test;
}`, 29)
    ])
  })

  it('Should tokenize chain operator', () => {
    let tokens = Tokenizer`.test:h1:"Test"`
    expect(tokens).to.deep.equal([
      new OperatorToken('.', 0),
      new IdentifierToken('test', 1),
      new OperatorToken(':', 5),
      new IdentifierToken('h1', 6),
      new OperatorToken(':', 8),
      new LiteralToken('Test', 9),
    ])
  })

  it('Should tokenize exclamation mark in identifier', () => {
    let tokens = Tokenizer`!DOCTYPE()`
    expect(tokens).to.deep.equal([
      new IdentifierToken('!DOCTYPE', 0),
      new SeperatorToken('(', 8),
      new SeperatorToken(')', 9),
    ])
  })

  it('Should tokenize piped text', () => {
    let tokens = Tokenizer`
      .foo
      | A
      | B
      | C
    `
    expect(tokens).to.deep.equal([
      new NewlineToken(0),
      new OperatorToken('.', 7),
      new IdentifierToken('foo', 8),
      new NewlineToken(11),
      new LiteralToken(' A\n B\n C\n', 18),
    ])
  })

  it('Should tokenize function pointer', () => {
    let foo = () => {}
    let tokens = Tokenizer`${foo}`
    expect(tokens).to.deep.equal([new TemplateValueToken(foo, 0)])
  })

  it('Should tokenize mixin with children', () => {
    const Mixin = (/** @type {any} */ children) => html` p { "test" ${children}
    }`
    let tokens = Tokenizer`.test { ${Mixin} { b: "bar" } }`
    expect(tokens).to.deep.equal([
      new OperatorToken('.', 0),
      new IdentifierToken('test', 1),
      new SeperatorToken('{', 6),
      new TemplateValueToken(Mixin, 8),
      new SeperatorToken('{', 10),
      new IdentifierToken('b', 12),
      new OperatorToken(':', 13),
      new LiteralToken('bar', 15),
      new SeperatorToken('}', 21),
      new SeperatorToken('}', 23),
    ])
  })

  it('Should tokenize array of mixins', () => {
    const Mixin = (/** @type {any} */ children) => html`p { "test" }`
    let tokens = Tokenizer`.test { ${[1,2,3,4].map(Mixin)} }`
    expect(tokens).to.deep.equal([
      new OperatorToken('.', 0),
      new IdentifierToken('test', 1),
      new SeperatorToken('{', 6),
      new TemplateValueToken([1,2,3,4].map(Mixin), 8),
      new SeperatorToken('}', 10),
    ])
  })
})
