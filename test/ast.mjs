import { expect } from 'chai'

import { Tokenizer } from '../src/tokenizer.mjs'
import { toAST, ASTify } from '../src/ast.mjs'
import { LiteralNode, RootNode, TagNode } from '../src/nodes.mjs'
import { html } from '../src/index.mjs'
import { cssattr, htmlattr } from './nodes.mjs'
import { AST2HTML } from '../src/toHTML.mjs'

describe('AST', () => {
  it('should parse to a simple ast', () => {
    let tokens = Tokenizer`foo(a="bar")`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([new TagNode('foo', [htmlattr('a', 'bar')], null, [])])
    )
  })
  it('should parse tag list', () => {
    let tokens = Tokenizer`foo(a="bar") bar(a="foo")`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('foo', [htmlattr('a', 'bar')], null, []),
        new TagNode('bar', [htmlattr('a', 'foo')], null, []),
      ])
    )
  })

  it('should parse nested tags', () => {
    let tokens = Tokenizer`foo(a="bar") { bar(a="foo") }`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('foo', [htmlattr('a', 'bar')], null, [
          new TagNode('bar', [htmlattr('a', 'foo')], null, []),
        ]),
      ])
    )
  })
  it('should parse literal node', () => {
    let tokens = Tokenizer`"Test"`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([new LiteralNode('Test', true, null, [])])
    )
  })
  it('should parse a nested literal node', () => {
    let tokens = Tokenizer`p() { "Test" }`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('p', [], null, [new LiteralNode('Test', true, null, [])]),
      ])
    )
  })
  it('should parse simple parallel nestings', () => {
    let tokens = Tokenizer`div { p { "A" } } div { }`
    let ast = toAST(tokens)

    //let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('div', [], null, [
          new TagNode('p', [], null, [new LiteralNode('A', true, null, [])]),
        ]),
        new TagNode('div', [], null, []),
      ])
    )
  })
  it('should parse parallel nestings', () => {
    let tokens = Tokenizer`div(class="a") { p { "A" } } div(class="b") { }`
    let astifyer = new ASTify(tokens)
    for (let i = 0; i < 18; i++) {
      astifyer.next()
    }
    let ast = astifyer.tree.ast

    //let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('div', [htmlattr('class', 'a')], null, [
          new TagNode('p', [], null, [new LiteralNode('A', true, null, [])]),
        ]),
        new TagNode('div', [htmlattr('class', 'b')], null, []),
      ])
    )
  })

  it('should parse multiple parallel nestings', () => {
    let tokens = Tokenizer`div(class="a") { p { "A" } } div(class="b") { p { "B" }}`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('div', [htmlattr('class', 'a')], null, [
          new TagNode('p', [], null, [new LiteralNode('A', true, null, [])]),
        ]),
        new TagNode('div', [htmlattr('class', 'b')], null, [
          new TagNode('p', [], null, [new LiteralNode('B', true, null, [])]),
        ]),
      ])
    )
  })

  it('Should parse classtag', () => {
    let tokens = Tokenizer`.test`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([new TagNode('div', [htmlattr('class', 'test')], null, [])])
    )
  })
  it('Should parse classtag with parameters', () => {
    let tokens = Tokenizer`.test (foo="bar")`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode(
          'div',
          [htmlattr('class', 'test'), htmlattr('foo', 'bar')],
          null,
          []
        ),
      ])
    )
  })
  it('Should parse idtag with parameters', () => {
    let tokens = Tokenizer`#test (foo="bar")`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode(
          'div',
          [htmlattr('id', 'test'), htmlattr('foo', 'bar')],
          null,
          []
        ),
      ])
    )
  })
  it('Should parse tag with class operator', () => {
    let tokens = Tokenizer`p.test`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([new TagNode('p', [htmlattr('class', 'test')], null, [])])
    )
  })
  it('Should parse tag with id operator', () => {
    let tokens = Tokenizer`p#test`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([new TagNode('p', [htmlattr('id', 'test')], null, [])])
    )
  })
  it('should parse this failing thing', () => {
    let tokens = Tokenizer`
  body {
    h1 {
      "Hallo"
    }
  }
`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('body', [], null, [
          new TagNode('h1', [], null, [new LiteralNode('Hallo', true, null)]),
        ]),
      ])
    )
  })
  it('Should parse template value inside tag', () => {
    let template_value = 'test'
    let tokens = Tokenizer`h1${template_value}`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([new TagNode('h1test', [], null, [])])
    )
  })

  it('Should parse template value inside nesting', () => {
    let template_value = 'test'
    let tokens = Tokenizer`h1 { ${template_value} }`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('h1', [], null, [new LiteralNode('test', false, null, [])]),
      ])
    )
  })

  it('Should parse template value inside argument as key or value', () => {
    let template_value = 'test'
    let tokens = Tokenizer`h1(${template_value}=${template_value})`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('h1', [htmlattr(template_value, template_value)], null, []),
      ])
    )
  })

  it('Should parse template value inside argument as key with value true', () => {
    let template_value = 'test'
    let tokens = Tokenizer`h1(${template_value})`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([new TagNode('h1', [htmlattr(template_value, true)], null, [])])
    )
  })

  it('Should parse nested chain operator', () => {
    let tokens = Tokenizer`h1:p`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('h1', [], null, [new TagNode('p', [], null, []).chain()]),
      ])
    )
  })

  it('Should parse parallel nested chain operator', () => {
    let tokens = Tokenizer`a:p h2`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('a', [], null, [new TagNode('p', [], null, []).chain()]),
        new TagNode('h2', [], null, []),
      ])
    )
  })

  it('Should parse another parallel nested chain operator', () => {
    let tokens = Tokenizer`a:p h2:p:p:"Test"`
    let ast = toAST(tokens)
    let expected_ast = new RootNode([
      new TagNode('a', [], null, [new TagNode('p', [], null, []).chain()]),
      new TagNode('h2', [], null, [
        new TagNode('p', [], null, [
          new TagNode('p', [], null, [
            new LiteralNode('Test', true, null, []).chain(),
          ]).chain(),
        ]).chain(),
      ]),
    ])
    expect(ast).to.deep.equal(expected_ast)
  })

  it('Should parse nested chain operator followed by normal {} nesting', () => {
    let tokens = Tokenizer`h1:p { a b }`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('h1', [], null, [
          new TagNode('p', [], null, [
            new TagNode('a', [], null, []),
            new TagNode('b', [], null, []),
          ]).chain(),
        ]),
      ])
    )
  })

  it('Should parse failing example', () => {
    let tokens = Tokenizer`html {
      head:title: "Test"
      body {
        h1: "Hello World"
      }
    }`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('html', [], null, [
          new TagNode('head', [], null, [
            new TagNode('title', [], null, [
              new LiteralNode('Test', true, null, []).chain(),
            ]).chain(),
          ]),
          new TagNode('body', [], null, [
            new TagNode('h1', [], null, [
              new LiteralNode('Hello World', true, null, []).chain(),
            ]),
          ]),
        ]),
      ])
    )
  })

  it('Should parse another failing example', () => {
    let tokens = Tokenizer`a {
      b:c {
        d1: "A"
      }
      e {
        d2: "1"
      }
    }`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('a', [], null, [
          new TagNode('b', [], null, [
            new TagNode('c', [], null, [
              new TagNode('d1', [], null, [
                new LiteralNode('A', true, null, []).chain(),
              ]),
            ]).chain(),
          ]),
          new TagNode('e', [], null, [
            new TagNode('d2', [], null, [
              new LiteralNode('1', true, null, []).chain(),
            ]),
          ]),
        ]),
      ])
    )
  })

  it('Should parse chained template value', () => {
    let tokens = Tokenizer`a:b:${'test'} c`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('a', [], null, [
          new TagNode('b', [], null, [
            new LiteralNode('test', false, null, []).chain(),
          ]).chain(),
        ]),
        new TagNode('c', [], null, []),
      ])
    )
  })

  it('Should parse list of values without key in tag', () => {
    let tokens = Tokenizer`!doctype(html)`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([new TagNode('!doctype', [htmlattr('html', true)], null, [])])
    )
  })

  it('Should parse tag where key is in paranthesis', () => {
    let tokens = Tokenizer`html("xml:foo"="bar")`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('html', [htmlattr('xml:foo', 'bar')], null, []),
      ])
    )
  })

  it('Should parse failing example of chain operator after tag node with attributes', () => {
    let tokens = Tokenizer`p(id="asd"): "blubb"`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('p', [htmlattr('id', 'asd')], null, [
          new LiteralNode('blubb', true, null, []).chain(),
        ]),
      ])
    )
  })

  it('Should astify pipe | operator as decendant of root node', () => {
    let tokens = Tokenizer`
      .foo
      | A
      | B
      | C
    `
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('div', [htmlattr('class', 'foo')], null, []),
        new LiteralNode(' A\n B\n C\n', true, null, []),
      ])
    )
  })
  it('Should astify template value of type object for tag node prop value', () => {
    let tokens = Tokenizer`
      div(class=${{ active: true }})
    `
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('div', [htmlattr('class', {'active': true})], null, []),
      ])
    )
  })
  it('Should astify template value of type array for tag node prop value', () => {
    let tokens = Tokenizer`
      div(class=${['foo', true && 'bar']})
    `
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('div', [htmlattr('class', ['foo', 'bar'])], null, []),
      ])
    )
  })

  it('Should parse chained template value that is a mixin', () => {
    const Mixin = () => `test`
    let tokens = Tokenizer`a:b:${Mixin} c`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('a', [], null, [
          new TagNode('b', [], null, [
            new TagNode('', [], null, []).chain().set_mixin(Mixin),
          ]).chain(),
        ]),
        new TagNode('c', [], null, []),
      ])
    )
  })

  it('Should parse mixin with children', () => {
    const Mixin = () => `test`
    let tokens = Tokenizer`${Mixin} { p: "test" }`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('', [], null, [
          new TagNode('p', [], null, [
            new LiteralNode('test', true, null, []).chain(),
          ]),
        ]).set_mixin(Mixin),
      ])
    )
  })

  it('Should parse mixin with children and descending mixin', () => {
    const Mixin = () => `test`
    let tokens = Tokenizer`
      ${Mixin} {
        p: "test"
      }
      ${Mixin}
    `
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('', [], null, [
          new TagNode('p', [], null, [
            new LiteralNode('test', true, null, []).chain(),
          ]),
        ]).set_mixin(Mixin),
        new TagNode('', [], null, []).set_mixin(Mixin),
      ])
    )
  })

  it('Should parse another failing example with mixins', () => {
    //@ts-ignore
    const Set = (name, path_mp3, path_cover) => html`
      .set { .name: ${name} br video(poster=${path_cover}) {
      source(src=${path_mp3} type="audio/mp3") "Your browser does not support
      the audio element." } .controls }
    `
    let tokens = Tokenizer`
      ${Set(
        'forest sounds [may 2021]',
        '/sets/forest sounds.mp3',
        '/sets/forest sounds.png'
      )}
      ${Set(
        'scars & memories [dec 2020]',
        '/sets/love distortion.mp3',
        '/sets/love distortion.png'
      )}
    `
    let ast = toAST(tokens)
  }).timeout(0)

  it('Should astify css attribute', () => {
    //@ts-ignore
    let tokens = Tokenizer`
.set(%display="flex" %color="red")`
    let ast = toAST(tokens)

    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode(
          'div',
          [
            htmlattr('class', 'set'),
            cssattr('display', 'flex'),
            cssattr('color', 'red'),
          ],
          null,
          []
        ),
      ])
    )
  }).timeout(0)

  it('should astify function pointer aka component', () => {
    let foo = () => {}
    //@ts-ignore
    let tokens = Tokenizer`${foo}(test="blubb")`
    let ast = toAST(tokens)

    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode('', [htmlattr('test', 'blubb')], null, []).set_mixin(foo),
      ])
    )
  })

  it('should astify mixin with class shorthand', () => {
    let foo = () => {}
    //@ts-ignore
    let tokens = Tokenizer`${foo}.someclass(test="blubb")`
    let ast = toAST(tokens)

    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode(
          '',
          [htmlattr('class', 'someclass'), htmlattr('test', 'blubb')],
          null,
          []
        ).set_mixin(foo),
      ])
    )
  })

  it('should astify mixin with true argument', () => {
    let foo = () => {}
    //@ts-ignore
    let tokens = Tokenizer`${foo}(hidden)`
    let ast = toAST(tokens)

    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode(
          '',
          [htmlattr('hidden', true)],
          null,
          []
        ).set_mixin(foo),
      ])
    )
  })

  it('should astify html attr after css attr', () => {
    let foo = () => {}
    //@ts-ignore
    let tokens = Tokenizer`p(%color="red" foo="bar")`
    let ast = toAST(tokens)

    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode(
          'p',
          [cssattr('color', 'red'), htmlattr('foo', 'bar')],
          null,
          []
        ),
      ])
    )
  })

  it('should astify mixin with children', () => {
    const Mixin = (/** @type {any} */ children) => html` p { "test" ${children}
    }`
    let tokens = Tokenizer`.test { ${Mixin} { b: "bar" } }`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode(
          'div',
          [htmlattr('class', 'test')],
          null,
          [
            new TagNode(
              '',
              [],
              null,
              [
                new TagNode(
                  'b',
                  [],
                  null,
                  [
                    new LiteralNode('bar', true).chain()
                  ]
                )
                
              ]
            ).set_mixin(Mixin)
          ]
        ),
      ])
    )
  })

  it('should astify template with mixins', () => {
    class Template {
      head = () => "Some Title"
      /** @type {any} */
      content = 'testtest'
      skeleton = () => Tokenizer`
        head: ${this.head}
        .content: ${this.content}
      `
    }

    // Test that mixins work too
    let n = new Template()
    n.content = () => html`p: "foobardeux"`

    let tokens = n.skeleton()
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode(
          'head',
          [],
          null,
          [
            new TagNode(
              '',
              [],
              null,
              []
            ).set_mixin(n.head).chain()
          ]
        ),
        new TagNode(
          'div',
          [htmlattr('class', 'content')],
          null,
          [
            new TagNode(
              '',
              [],
              null,
              []
            ).set_mixin(n.content).chain()
          ]
        ),
      ])
    )
  })
  
  it('should astify array expansion', () => {
    const Mixin = (/** @type {any} */ num) => html`p { ${num} }`
    let tokens = Tokenizer`.test { ${[1,2,3,4].map(Mixin)} }`
    let ast = toAST(tokens)
    expect(ast).to.deep.equal(
      new RootNode([
        new TagNode(
          'div',
          [htmlattr('class', 'test')],
          null,
          [
            new LiteralNode(Mixin(1), false, null),
            new LiteralNode(Mixin(2), false, null),
            new LiteralNode(Mixin(3), false, null),
            new LiteralNode(Mixin(4), false, null),
          ]
        ),
      ])
    )
  })
})
