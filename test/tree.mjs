import { expect } from 'chai'
import { RootNode, TagNode } from '../src/nodes.mjs'
import { Tree } from '../src/tree.mjs'

describe('Tree', () => {
  it('when inserting a chain node should add correctly', () => {
    const tree = new Tree()
    expect(tree.ast).to.deep.equal(new RootNode([]))
    tree.climbToNode(tree.insertNode(new TagNode('div', [], null, [])))
    expect(tree.ast).to.deep.equal(
      new RootNode([new TagNode('div', [], null, [])])
    )
    tree.climbToNode(tree.insertNode(new TagNode('p', [], null, []).chain()))
    expect(tree.ast).to.deep.equal(
      new RootNode([
        new TagNode('div', [], null, [new TagNode('p', [], null, []).chain()]),
      ])
    )
    tree.climbToNode(tree.insertNode(new TagNode('b', [], null, []).chain()))
    expect(tree.ast).to.deep.equal(
      new RootNode([
        new TagNode('div', [], null, [
          new TagNode('p', [], null, [
            new TagNode('b', [], null, []).chain(),
          ]).chain(),
        ]),
      ])
    )

    tree.insertNode(new TagNode('c', [], null, []))
    tree.insertNode(new TagNode('d', [], null, []))
    expect(tree.ast).to.deep.equal(
      new RootNode([
        new TagNode('div', [], null, [
          new TagNode('p', [], null, [
            new TagNode('b', [], null, [
              new TagNode('c', [], null, []),
              new TagNode('d', [], null, []),
            ]).chain(),
          ]).chain(),
        ]),
      ])
    )

    tree.climbDown()

    tree.climbToNode(tree.insertNode(new TagNode('p2', [], null, [])))
    expect(tree.ast).to.deep.equal(
      new RootNode([
        new TagNode('div', [], null, [
          new TagNode('p', [], null, [
            new TagNode('b', [], null, [
              new TagNode('c', [], null, []),
              new TagNode('d', [], null, []),
            ]).chain(),
          ]).chain(),
        ]),
        new TagNode('p2', [], null, []),
      ])
    )
  })
})
