import { expect } from 'chai'

import { Tokenizer } from '../src/tokenizer.mjs'
import { toAST } from '../src/ast.mjs'
import { AST2HTML } from '../src/toHTML.mjs'
import { html } from '../src/index.mjs'
import { spawn as spawnSync } from 'child_process';

import { formatterFactory, HtmlValidate } from 'html-validate'

/**
 * @param {string} command
 * @param {string[]} args
 */
function spawn(command, ...args) {
  let p = spawnSync(command, args)
  return new Promise((resolve) => {
    let collect = ''
    p.stdout.on("data", (x) => {
      collect += x.toString();
    });
    p.stderr.on("data", (x) => {
      collect += x.toString();
    });
    p.on("exit", (code) => {
      resolve(collect);
    });
  });

}

/**
 * @param {string} exampleFile
 */
export async function runExampleFile(exampleFile) {
  let commandOutput = await spawn('node', exampleFile)
  return commandOutput
}

describe('examples', () => {
  it('examples/pipe-operator.mjs', async () => {
    let output = await runExampleFile('./examples/pipe-operator.mjs')
    expect(output).to.eq("<p>This is Line 1\nThis is Line 2\n\nThis is Line 4\n</p>\n")
  })

  it('examples/mixins.mjs', async () => {
    let output = await runExampleFile('./examples/mixins.mjs')
    expect(output).to.eq(`<!DOCTYPE html><body><div style="width:100px;height:100px;background-color:red" class="Rectangle">{&quot;abc&quot;:&quot;blubb&quot;}test1</div><div style="width:100px;height:100px;background-color:blue" class="Rectangle">{&quot;abc&quot;:&quot;blubb&quot;}test2</div></body>\n`)
  })

  it('examples/simple-page.js', async () => {
    let output = await runExampleFile('./examples/simple-page.mjs')
    expect(output).to.eq(`<!DOCTYPE html><html><head><title>Test</title><style>
        body {
          background-color: red;
        }
        table {
          background-color: green;
        }</style></head><body><h1>Hello World</h1><p>Test</p><table><thead><tr><th>A</th><th>B</th><th>C</th></tr></thead><tr><td>1</td><td>as</td><td>fölk</td></tr></table></body></html>
`)
  })
})
