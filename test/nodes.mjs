import { expect } from 'chai'

import { LiteralNode, RootNode, TagNode } from '../src/nodes.mjs'

/**
 * @param {string} key
 * @param {(string|boolean|object)[]} values
 * @returns {import('../src/nodes.mjs').TagNodeProp}
 */
export function htmlattr(key, ...values) {
  return {
    type: 'html',
    key,
    values,
  }
}

/**
 * @param {string} key
 * @param {string[]} values
 * @returns {import('../src/nodes.mjs').TagNodeProp}
 */
export function cssattr(key, ...values) {
  return {
    type: 'css',
    key,
    values,
  }
}

describe('Nodes', () => {
  it('test node structure', () => {
    const ast = new RootNode([
      new TagNode('foo', [htmlattr('a', 'bar')], null, [
        new LiteralNode('testcontent', true, null, []),
      ]),
    ])
    const rootNode = {
      type: 'root',
      children: [],
      parentNode: null,
      isChained: false,
    }
    const tagNode = {
      type: 'tag',
      children: [],
      parentNode: rootNode,
      isChained: false,
      props: [htmlattr('a', 'bar')],
      tag: 'foo',
      mixin: null,
    }

    const literalNode = {
      type: 'literal',
      children: [],
      parentNode: tagNode,
      isChained: false,
      encode: true,
      content: 'testcontent',
    }

    //@ts-ignore
    tagNode.children.push(literalNode)

    //@ts-ignore
    rootNode.children.push(tagNode)
    expect(ast).to.deep.equal(rootNode)
    let rootNodeToString = ast.toString()
    expect(rootNodeToString).to.equal(
      `RootNode { parentNode: null, children: [
  TagNode { tag: "foo", props: [{"type":"html","key":"a","values":["bar"]}], isChained: false, mixin: null, children: [
    LiteralNode { content: "testcontent", encode: true, isChained: false, children: [] }
  ] }
] }`
    )
  })
  it('test node structure with multiple children', () => {
    const ast = new RootNode([
      new TagNode('div', [], null, [
        new TagNode('foo1', [htmlattr('a', 'bar')], null, []),
        new TagNode('foo2', [htmlattr('b', 'bar')], null, []),
      ]),
    ])

    /** @type {RootNode} */
    const rootNode = new RootNode([])

    /** @type {TagNode} */
    const divNode = new TagNode('div', [], rootNode)
    /** @type {TagNode} */
    const tagNode1 = new TagNode('foo1', [htmlattr('a', 'bar')], divNode)
    /** @type {TagNode} */
    const tagNode2 = new TagNode('foo2', [htmlattr('b', 'bar')], divNode)

    divNode.children.push(tagNode1)
    divNode.children.push(tagNode2)

    rootNode.children.push(divNode)

    expect(ast).to.deep.equal(rootNode)
    let rootNodeToString = ast.toString()
    expect(rootNodeToString).to.equal(
      `RootNode { parentNode: null, children: [
  TagNode { tag: "div", props: [], isChained: false, mixin: null, children: [
    TagNode { tag: "foo1", props: [{"type":"html","key":"a","values":["bar"]}], isChained: false, mixin: null, children: [] }
    TagNode { tag: "foo2", props: [{"type":"html","key":"b","values":["bar"]}], isChained: false, mixin: null, children: [] }
  ] }
] }`
    )
  })
  it('test node structure with multiple children and different nesting levels', () => {
    const ast = new RootNode([
      new TagNode('div', [], null, [
        new TagNode('foo1', [htmlattr('a', 'bar')], null, [
          new TagNode('p', [], null, []),
        ]),
        new TagNode('foo2', [htmlattr('b', 'bar')], null, []),
      ]),
    ])
    /** @type {RootNode} */
    const rootNode = new RootNode([])

    /** @type {TagNode} */
    const divNode = new TagNode('div', [], rootNode)

    /** @type {TagNode} */
    const tagNode1 = new TagNode('foo1', [htmlattr('a', 'bar')], divNode)

    /** @type {TagNode} */
    const pNode = new TagNode('p', [], tagNode1)
    tagNode1.children.push(pNode)

    /** @type {TagNode} */
    const tagNode2 = new TagNode('foo2', [htmlattr('b', 'bar')], divNode)

    divNode.children.push(tagNode1)
    divNode.children.push(tagNode2)

    rootNode.children.push(divNode)

    expect(ast).to.deep.equal(rootNode)
    let rootNodeToString = ast.toString()
    expect(rootNodeToString).to.equal(
      `RootNode { parentNode: null, children: [
  TagNode { tag: "div", props: [], isChained: false, mixin: null, children: [
    TagNode { tag: "foo1", props: [{"type":"html","key":"a","values":["bar"]}], isChained: false, mixin: null, children: [
      TagNode { tag: "p", props: [], isChained: false, mixin: null, children: [] }
    ] }
    TagNode { tag: "foo2", props: [{"type":"html","key":"b","values":["bar"]}], isChained: false, mixin: null, children: [] }
  ] }
] }`
    )
  })
})
