import { expect } from 'chai'

import {
  extractLineAtPos,
  extractLines,
  formatLineWithError,
} from '../src/error.mjs'
import { html } from '../src/index.mjs'
import { reflect } from 'nox-reflect'
import path from 'node:path'

describe('Error', () => {
  describe('extractLineAtPos', () => {
    it('should extract the correct line', () => {
      const data = `foo(a="bar)`
      const extract_lines = extractLineAtPos(data, 10)
      expect(extract_lines).to.deep.equal({
        line_number: 1,
        column: 11,
        text: 'foo(a="bar)',
        start_pos: 0,
        end_pos: 11,
      })
    })

    it('should extract the correct line and calculate line number and column for a multiline string', () => {
      const data = `foo(a="bar") {
  p {
    b("asd")
  }
}`
      const extract_lines = extractLineAtPos(data, 0)
      expect(extract_lines).to.deep.equal({
        line_number: 1,
        column: 1,
        text: 'foo(a="bar") {',
        start_pos: 0,
        end_pos: 14,
      })
    })

    it('should extract the correct line and calculate line number and column for a multiline string', () => {
      const data = `foo(a="bar") {
  p {
    b("asd")
  }
}`
      const extract_lines = extractLineAtPos(data, 16)
      expect(extract_lines).to.deep.equal({
        line_number: 2,
        column: 2,
        text: '  p {',
        start_pos: 15,
        end_pos: 20,
      })
    })
  })

  describe('extractLines', () => {
    it('should only extract the main line', () => {
      const data = `foo(a="bar)`
      const POS = 6
      const extract_lines = extractLines(data, POS)
      expect(data.slice(POS, POS + 3)).to.equal(`"ba`)
      expect(extract_lines).to.deep.equal({
        before: null,
        main: {
          line_number: 1,
          column: POS + 1,
          text: 'foo(a="bar)',
          start_pos: 0,
          end_pos: 11,
        },
        after: null,
      })
    })

    it('should extract the correct lines for a multiline string', () => {
      const data = `foo(a="bar") {
  p {
    b("asd")
  }
}`
      const POS = 17
      const extract_lines = extractLines(data, POS)
      expect(data.slice(POS, POS + 3)).to.equal(`p {`)
      expect(extract_lines).to.deep.equal({
        before: {
          line_number: 1,
          column: 1,
          text: 'foo(a="bar") {',
          start_pos: 0,
          end_pos: 14,
        },
        main: {
          line_number: 2,
          column: 3,
          text: '  p {',
          start_pos: 15,
          end_pos: 20,
        },
        after: {
          line_number: 3,
          column: 1,
          text: '    b("asd")',
          start_pos: 21,
          end_pos: 33,
        },
      })
    })
    it('should extract the correct lines for a multiline string with offsetting', () => {
      const data = `foo(a="bar") {
  p {
    b("asd")
  }
}`
      const POS = 17
      const extract_lines = extractLines(data, POS, 3)
      expect(data.slice(POS, POS + 3)).to.equal(`p {`)
      expect(extract_lines).to.deep.equal({
        before: {
          line_number: 4,
          column: 1,
          text: 'foo(a="bar") {',
          start_pos: 0,
          end_pos: 14,
        },
        main: {
          line_number: 5,
          column: 3,
          text: '  p {',
          start_pos: 15,
          end_pos: 20,
        },
        after: {
          line_number: 6,
          column: 1,
          text: '    b("asd")',
          start_pos: 21,
          end_pos: 33,
        },
      })
    })
  })

  describe('formatLineWithError', () => {
    it('should render a beautiful error', () => {
      const data = `foo(a="bar") {
  p {
    b("asd")
  }
}`
      const POS = 17
      const result = formatLineWithError(
        'src/foo.ts',
        data,
        POS,
        'This is a test error',
        ''
      )
      expect(data.slice(POS, POS + 3)).to.equal(`p {`)
      expect(result).to.equal(
        `    --> src/foo.ts:2:3
    1 | foo(a="bar") {
    2 |   p {
      |   ^ This is a test error
    3 |     b("asd")
`
      )
    })
    it('should render another beautiful error', () => {
      const data = `foo(a="bar") {
  p {
    b("asd")
  }
}`
      const POS = 25
      const result = formatLineWithError(
        'src/bar.ts',
        data,
        POS,
        'This is another test error',
        ''
      )
      expect(data.slice(POS, POS + 3)).to.equal(`b("`)
      expect(result).to.equal(
        `    --> src/bar.ts:3:5
    2 |   p {
    3 |     b("asd")
      |     ^ This is another test error
    4 |   }
`
      )
    })
    it('should render beautiful error with correct line offsetting', () => {
      const data = `foo(a="bar") {
  p {
    b("asd")
  }
}`
      const POS = 25
      const result = formatLineWithError(
        'src/bar.ts',
        data,
        POS,
        'This is another test error',
        '',
        2
      )
      expect(data.slice(POS, POS + 3)).to.equal(`b("`)
      expect(result).to.equal(
        `    --> src/bar.ts:5:5
    4 |   p {
    5 |     b("asd")
      |     ^ This is another test error
    6 |   }
`
      )
    })
  })

  describe('NoxHTMLError', () => {
    it('should throw nox error', () => {
      const reflect_object = reflect(1)
      if (reflect_object === null) throw new Error('ReflectObject is null??')
      const __dirname = path.dirname(reflect_object.file || '')
      const line_number = 230

      expect(() => html`a:"foo" test(a=`).to.throw(`
    --> ${path.join(__dirname, 'error.mjs')}:${line_number}:15
    ${line_number} | a:"foo" test(a=
        |               ^ Error at position 14: TAG_PROPS_EQUALS is an invalid state at the EOF
`)
    })
  })
})
