import { expect } from 'chai'

import { Tokenizer } from '../src/tokenizer.mjs'
import { toAST } from '../src/ast.mjs'
import { AST2HTML } from '../src/toHTML.mjs'
import { html } from '../src/index.mjs'

import { formatterFactory, HtmlValidate } from 'html-validate'

/**
 * @param {string} markup
 */
export function expectValidHTML(markup) {
  const htmlValidate = new HtmlValidate({})
  const report = htmlValidate.validateString(markup)
  if (!report.valid) {
    const text = formatterFactory('text')
    if (text) throw new Error(text(report.results))
  }
}

describe('toHTML', () => {
  it('should generate html from a simple tag', () => {
    let tokens = Tokenizer`div(a="bar")`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<div a="bar"></div>')
    expectValidHTML(htmlString)
  })

  it('should generate html from a simple tag list', () => {
    let tokens = Tokenizer`div(a="bar")p()`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<div a="bar"></div><p></p>')
    expectValidHTML(htmlString)
  })
  it('should generate html from a simple nested tag', () => {
    let tokens = Tokenizer`div(a="bar") { p() }`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<div a="bar"><p></p></div>')
    expectValidHTML(htmlString)
  })

  it('should generate html from a simple nested literal tag', () => {
    let tokens = Tokenizer`div(a="bar") { p() { "test" } }`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<div a="bar"><p>test</p></div>')
    expectValidHTML(htmlString)
  })

  it('should generate html from a simple nested literal tag and escape things in literal tag', () => {
    let tokens = Tokenizer`div(a="bar") { p() { "test < > \\" \' & © ∆" } }`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal(
      '<div a="bar"><p>test &lt; &gt; \\&quot; &apos; &amp; © ∆</p></div>'
    )
    expectValidHTML(htmlString)
  })

  it('should generate html from a simple tag without parameters', () => {
    let tokens = Tokenizer`span`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<span></span>')
    expectValidHTML(htmlString)
  })
  it('should generate html from a simple nested tag without parameters', () => {
    let tokens = Tokenizer`p { "Test" } `
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<p>Test</p>')
    expectValidHTML(htmlString)
  })
  it('should generate html from multiple parallel nestings', () => {
    let tokens = Tokenizer`div(class="a") { p { "A" } } div(class="b") { p { "B" }}`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal(
      '<div class="a"><p>A</p></div><div class="b"><p>B</p></div>'
    )
    expectValidHTML(htmlString)
  })
  it('should generate html from simple class tag', () => {
    let tokens = Tokenizer`.test`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<div class="test"></div>')
    expectValidHTML(htmlString)
  })
  it('should generate html from simple class tag with multiple class operators', () => {
    let tokens = Tokenizer`.foo.bar`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<div class="foo bar"></div>')
    expectValidHTML(htmlString)
  })

  it('should generate html from template value inside tag', () => {
    let template_value = '5'
    let tokens = Tokenizer`h${template_value}: "test"`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<h5>test</h5>')
    expectValidHTML(htmlString)
  })
  it('should generate html from template value inside nesting', () => {
    let template_value = 'test'
    let tokens = Tokenizer`h1 { ${template_value} }`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<h1>test</h1>')
    expectValidHTML(htmlString)
  })

  it('should generate html from template value inside argument as key or value', () => {
    let template_value = 'test'
    let tokens = Tokenizer`div(${template_value}=${template_value})`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<div test="test"></div>')
    expectValidHTML(htmlString)
  })

  it('should generate html from template value inside argument as key without value', () => {
    let template_value = 'test'
    let tokens = Tokenizer`div(${template_value})`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<div test></div>')
    expectValidHTML(htmlString)
  })

  it('should generate html from tag with disabled argument without "="', () => {
    let tokens = Tokenizer`div(disabled)`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<div disabled></div>')
    expectValidHTML(htmlString)
  })

  it('should not encode template value that get understood as a literal node', () => {
    let tokens = Tokenizer`div { ${'<p>Test</p>'} }`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<div><p>Test</p></div>')
    expectValidHTML(htmlString)
  })

  it('should generate html from piped text', () => {
    let tokens = Tokenizer`p {
                             | This is
                             | a test
                           }`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<p> This is\n a test\n</p>')
    expectValidHTML(htmlString)
  })

  it('should do the !DOCTYPE constructor', () => {
    let tokens = Tokenizer`!DOCTYPE(html)`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<!DOCTYPE html>')
    expectValidHTML(htmlString)
  })
  it('should not fail on weird doctype tag', () => {
    let tokens = Tokenizer`!DOCTYPE(html public "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd")`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal(
      '<!DOCTYPE html public "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">'
    )
    //expectValidHTML(htmlString)
  })

  it('should execute mixin if passed as template variable', () => {
    let tokens = Tokenizer`p { ${() => 'test'}}`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<p>test</p>')
    expectValidHTML(htmlString)
  })

  it('should generate html for mixin with children', () => {
    const Mixin = (/** @type {any} */{ children}) => html` p { "test" ${children}`
    let tokens = Tokenizer`.test { ${Mixin} { b: "bar" } }`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<div class="test"><p>test<b>bar</b></p></div>')
    expectValidHTML(htmlString)
  })

  it('should correctly htmlify css attribute', () => {
    let tokens = Tokenizer`p(%color="red"): "test"`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<p style="color:red">test</p>')
    expectValidHTML(htmlString)
  })

  it('should correctly htmlify multiple css attributes', () => {
    let tokens = Tokenizer`p(%color="red" %fontSize="12px")`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<p style="color:red;font-size:12px"></p>')
    expectValidHTML(htmlString)
  })

  it('should htmlify a html attribute after a css attribute', () => {
    let tokens = Tokenizer`p(%color="red", foo="bar")`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<p style="color:red" foo="bar"></p>')
    expectValidHTML(htmlString)
  })
  
  it('should htmlify a html attribute after a css attribute', () => {
    let tokens = Tokenizer`p(%color="red", style="background-color: blue")`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<p style="color:red;background-color:blue"></p>')
    expectValidHTML(htmlString)
  }) 

  it('should htmlify templates with the class pattern', () => {
    class Template {
      head = () => html`
        title: "Some Title"
        meta(charset='utf-8')
      `
      content = 'testtest'
      skeleton = () => html`
        !DOCTYPE(html)
        html(xmlns="http://www.w3.org/1999/xhtml" lang="de" "xml:lang"="de") {
          head: ${this.head}
          body {
            .content: ${this.content}
          }
        }
      `
      toString() {
        return html`${this.skeleton}`
      }
    }

    let t = new Template()
    expect(t.toString()).to.equal('<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" lang="de" xml:lang="de"><head><title>Some Title</title><meta charset="utf-8"/></head><body><div class="content">testtest</div></body></html>')

    // Test that overwriting works too
    let p = new Template()
    p.content = html`p: "foobar"`

    expect(p.toString()).to.equal('<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" lang="de" xml:lang="de"><head><title>Some Title</title><meta charset="utf-8"/></head><body><div class="content"><p>foobar</p></div></body></html>')
  })
  it('should htmlify templates with mixins', () => {
    class Template {
      head = () => html`
        title: "Some Title"
        meta(charset='utf-8')
      `
      header = ''
      /** @type {any} */
      content = 'testtest'
      skeleton = () => html`
        !DOCTYPE(html)
        html(xmlns="http://www.w3.org/1999/xhtml" lang="de" "xml:lang"="de") {
          head: ${this.head}
          body {
            ${this.header}
            .content: ${this.content}
          }
        }
      `
      toString() {
        return html`${this.skeleton}`
      }
    }

    // Test that mixins work too
    let n = new Template()
    n.content = () => html`p: "foobardeux"`

    expect(n.toString()).to.equal('<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" lang="de" xml:lang="de"><head><title>Some Title</title><meta charset="utf-8"/></head><body><div class="content"><p>foobardeux</p></div></body></html>')
  })
  it('should htmlify templates with template values', () => {
    class Template {
      head = () => html`
        title: "Some Title"
        meta(charset='utf-8')
      `
      header = ''
      content = 'testtest'
      skeleton = () => html`
        !DOCTYPE(html)
        html(xmlns="http://www.w3.org/1999/xhtml" lang="de" "xml:lang"="de") {
          head: ${this.head}
          body {
            ${this.header}
            .content: ${this.content}
          }
        }
      `
      toString() {
        return html`${this.skeleton}`
      }
    }

    // Test that mixins work too
    let n = new Template()
    n.content = html`p: "foobardeux"`

    expect(n.toString()).to.equal('<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" lang="de" xml:lang="de"><head><title>Some Title</title><meta charset="utf-8"/></head><body><div class="content"><p>foobardeux</p></div></body></html>')
  })

  it('should htmlify array expansion', () => {
    const Mixin = (/** @type {any} */ num) => html`p { ${num} }`
    let tokens = Tokenizer`.test { ${[1,2,3,4].map(Mixin)} }`
    let ast = toAST(tokens)
    let htmlString = AST2HTML(ast)
    expect(htmlString).to.equal('<div class="test"><p>1</p><p>2</p><p>3</p><p>4</p></div>')
    expectValidHTML(htmlString)
  })
})
