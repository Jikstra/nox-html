<div align="right">
    <a href="https://codeberg.org/jikstra/nox-html/releases"><img src="https://codeberg.org/jikstra/nox-html/raw/branch/main/assets/version-badge.svg" alt="gpl license badge" width="120px"/></a>
    <a href="https://www.gnu.org/licenses/gpl-3.0"><img src="https://codeberg.org/jikstra/nox-html/raw/branch/main/assets/license-badge.png" alt="gpl license badge" width="120px"/></a>
</div>
<br>
<br>
<div align="center">
  <h1>nox-html</h1>
</div>
<br>
<br>

nox-html is a template engine combining the ideas of pug.js + maud into a solution that fully works inside template literals. It's main focus is using it in static site generators.

ATTENTION: While I successfully use this in a couple of projects, this is still beta.


## Examples

### Hello World
```js
import {html} from 'nox-html'

console.log(html`
  head {
    title: "Hello World Example"
  }
  body {
    h1: "Hello World"
  }
`)
```

### Mixins
```js
import {html} from 'nox-html'

const Button = ({href, children}) => html`.button:a(href=${href}): ${children}`

console.log(html`
  p: "Some button"
  ${Button}(href="https://codeberg.org"): "Name of it"
`)
```

