# CHANGELOG

## Unreleased

## v3.0.1 -- 05.02.2025
- Fix author in package.json

## v3.0.0 -- 05.02.2025
- Array expansion
- Breaking: Don't try to guess indentation with piped text

## v2.0.0
- Fix mixins
