# Documentation

## 1. A simple Hello World
```js
import { html } from 'nox-html'

console.log(html`
  p: "Hello World"
`)
```

## 2. Nesting
```js
import { html } from 'nox-html'

console.log(html`
  div(class="wrapperclass") {
    h1: "HeadlineOne"
    p: "Some text"
  }
`)
```

## 2. Attributes
```js
import { html } from 'nox-html'

console.log(html`
  a(href="http://codeberg.org") {
    img(src="someimg.png" alt="This is some image")
  }
`)
```

## 3. Quick Nesting
```js
import { html } from 'nox-html'

console.log(html`
  a(href="http://codeberg.org") : img(src="someimg.png" alt="This is some image")
`)
```

## 4. Using variables
```js
import { html } from 'nox-html'

let some_variable = "blubb"

console.log(html`
  p : "${some_variable}"
  div(class="${some_variable}")
`)
```



## 5. Shorthand CSS attributes

Nox provides a neat short cut to have style attributes
```js
import { html } from 'nox-html'

console.log(html`
  div(%width="100px" %height="100px" %backgroundColor="red"): "Hello World"
`)
```

of course you can do the same with the classic style attribute:
```js
import { html } from 'nox-html'

console.log(html`
  div(style="width:100px;height:100px;backgroundColor:red"): "Hello World"
`)
```


## 6. Mixins
```js
import { html } from 'nox-html'

const Foo = ({children,style,...props}) => html`
  .Rectangle(style=${style}) {
    "${JSON.stringify(props)}"
    ${children}
  }
`

console.log(html`
  ${Foo}(%backgroundColor="red", abc="blubb"): "test"
`)
```

## 7. Multiline Strings
```js
import { html } from 'nox-html'

console.log(html`
  p:
    | This is line 1
    | This is also Line 1
    |
    | This is Line 2
`);
```

## 8. Conditionals
```js
import { html } from 'nox-html'

console.log(html`
  h1: "This is some headline"
  .wrapper {
    ${if (true) {
      html`p: "True"`
    } else {
      html`p: "False"`
    }}
  }
`);
```

