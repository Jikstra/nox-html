import {html } from '../src/index.mjs'

const Rectangle = ({children,style,...props}) => {
  return html`
    .Rectangle(%width="100px" %height="100px" style=${style}) {
      "${JSON.stringify(props)}"
      ${children}
    }
  `
}

console.log(html`
  !DOCTYPE(html)
  body {
    ${[
      ["test1", "red"],
      ["test2", "blue"]
    ].map(([text, backgroundColor]) => html`${Rectangle}(%backgroundColor=${backgroundColor}, abc="blubb"): ${text}`)}
  }`)

