import { html } from '../src/index.mjs'

function main() {
  return html` a { b:c { d1: "A" } e { d2: "1" } } `;
}

console.log(html`
  p:
    |This is Line 1
    |This is Line 2
    |
    |This is Line 4
`);
