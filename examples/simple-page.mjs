import { html } from "../src/index.mjs";

const page = html`
  !DOCTYPE(html)
  html {
    head {
      title: "Test"
      style: ${`
        body {
          background-color: red;
        }
        table {
          background-color: green;
        }`
      }
    }
    body {
      h1: "Hello World"
      p: "Test"
      table {
        thead:tr {
          th: "A"
          th: "B"
          th: "C"
        }
        tr {
          td: "1"
          td: "as"
          td: "fölk"
        }
      }
    }
  }
`

console.log(page);
