#!/usr/bin/env bash
set -xe

VERSION="$1"

jq ".version=\"$VERSION\"" package.json | sponge package.json

npm i

curl "https://img.shields.io/badge/version-$VERSION-pink" > assets/version-badge.svg

sed -i "/## Unreleased/a ## v$VERSION -- ??" CHANGELOG.md

nvim CHANGELOG.md

git add assets/version-badge.svg
git add package.json
git add package-lock.json
git add CHANGELOG.md

git commit -m "v$VERSION"
git tag -a "v$VERSION" -m "v$VERSION" 

echo "To push new version & tag execute 'git push origin main --tags'"
