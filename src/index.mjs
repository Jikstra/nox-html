import { Tokenizer } from './tokenizer.mjs'
import { toAST } from './ast.mjs'
import { AST2HTML } from './toHTML.mjs'
import { NoxHTMLError } from './error.mjs'



export class NoxHTML {
  /** @type {import('./ast.mjs').AST} */
  ast = null

  /** @type {import('./tokenizer.mjs').Token[]} */
  tokens = []

  constructor(
    /** @type {TemplateStringsArray} */ strings,
    /** @type {any[]} */ ...values
  ) {
    try {
      this.tokens = Tokenizer(strings, ...values)
      this.ast = toAST(this.tokens)
    } catch (err) {
      if (err instanceof NoxHTMLError) {
        err.beautify(html, 4, strings, ...values)
      }
      throw err
    }
  }

  toString() {
    if (this.ast === null) {
      throw new Error('this.ast is null')
    }
    return AST2HTML(this.ast)
  }
}

/**
 * @param {TemplateStringsArray} strings
 * @param {any[]} values
 * @returns {any}
 */
export function html(strings, ...values) {
  return new NoxHTML(strings, ...values).toString()
}
