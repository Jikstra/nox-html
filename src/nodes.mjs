import { structuredClone } from './utils.mjs'

/** @interface */
export class ASTNode {
  /** @type {string} */
  type = 'unimplemented'

  /** @type {ASTNode[]} */
  children = []

  /** @type {ASTNode | null} */
  parentNode = null

  /** @type {boolean} */
  isChained = false

  /** @returns {any} */
  clone() {
    throw new Error('Unimplemented')
  }

  /**
   * @param {boolean} [truncateChildren]
   * @param {number} [depth]
   * @returns {string}
   */
  // eslint-disable-next-line no-unused-vars
  toString(truncateChildren, depth) {
    throw new Error('Unimplemented')
  }
}

export class ChainableNode {
  /**
   * @param {boolean} [isChained=true]
   * @returns {ASTNode & ChainableNode}
   */
  // eslint-disable-next-line no-unused-vars
  chain(isChained = true) {
    throw new Error('Unimplemented')
  }
}

/**
 * @param {boolean} truncateChildren
 * @param {number} depth
 * @param {string} name
 * @param {any} args
 * @param {ASTNode[]} children
 * @returns {string}
 */
export function nodeFormatter(truncateChildren, depth, name, args, children) {
  let childrenToString = '[truncated]'
  const indent = '  '
  if (truncateChildren === false) {
    childrenToString =
      children.length === 0
        ? '[]'
        : '[\n' +
          children
            .map(c => c.toString(truncateChildren, depth + 1))
            .join('\n') +
          `\n${indent.repeat(depth)}]`
  }

  const argsString = Object.keys(args)
    .map(a => {
      let value = args[a]
      let value_str = null
      if (typeof value === 'function') {
        value_str = '[Function]'
      } else {
        value_str = JSON.stringify(args[a])
      }
     return `${a}: ${value_str}` 
    })
    .join(', ')
  return `${indent.repeat(
    depth
  )}${name} { ${argsString}, children: ${childrenToString} }`
}

/** @implements ASTNode */
export class RootNode {
  /** @type {ASTNode['type']} */
  type = 'root'

  /** @type {ASTNode['children']} */
  children = []

  /** @type {ASTNode['parentNode']} */
  parentNode = null

  /** @type {ASTNode['isChained']} */
  isChained = false

  /**
   * @param {RootNode['children']} children
   */
  constructor(children) {
    this.isChained = false
    if (children) {
      children = children.map(child => {
        child.parentNode = this
        return child
      })
      this.children = children
    }
  }

  /** @returns {RootNode} */
  clone() {
    return new RootNode(this.children)
  }

  /** @returns {string} */
  toString(truncateChildren = false, depth = 0) {
    return nodeFormatter(
      truncateChildren,
      depth,
      'RootNode',
      { parentNode: this.parentNode },
      this.children
    )
  }
}

/** @typedef {null|function} Mixin */

/**
 * @implements ChainableNode
 * @implements ASTNode
 */
export class TagNode {
  /** @type {ASTNode['type']} */
  type = 'tag'

  /** @type {ASTNode['children']} */
  children = []

  /** @type {ASTNode['parentNode']} */
  parentNode = null

  /** @type {ASTNode['isChained']} */
  isChained = false

  /** @type {string} */
  tag = ''

  /** @type {Mixin} */
  mixin = null

  /** @type {TagNodeProp[]} */
  props = []

  /**
   * @param {TagNode['tag']} tag
   * @param {TagNode['props']} props
   * @param {ASTNode['parentNode']} parentNode
   * @param {ASTNode['children']} children
   */
  constructor(tag, props, parentNode, children = []) {
    this.parentNode = parentNode
    this.children = mapChildrenToParentNode(children, this)
    this.tag = tag
    this.props = props
    this.isChained = false
  }

  /**
   * @returns {TagNode}
   */
  clone() {
    return new TagNode(this.tag, structuredClone(this.props), this.parentNode)
      .chain(this.isChained)
      .set_mixin(this.mixin)
  }

  /**
   * @returns {string}
   */
  toString(truncateChildren = false, depth = 0) {
    return nodeFormatter(
      truncateChildren,
      depth,
      'TagNode',
      {
        tag: this.tag,
        props: this.props,
        isChained: this.isChained,
        mixin: this.mixin,
      },
      this.children
    )
  }

  /**
   * @param {boolean} [isChained=true]
   * @returns {TagNode}
   */
  chain(isChained = true) {
    this.isChained = isChained
    return this
  }
  /**
   * @param {TagNode['mixin']} mixin
   * @returns {TagNode}
   */
  set_mixin(mixin) {
    this.mixin = mixin
    return this
  }
}

/**
 * @implements ChainableNode
 * @implements ASTNode
 */
export class LiteralNode {
  /** @type {ASTNode['type']} */
  type = 'literal'

  /** @type {ASTNode['children']} */
  children = []

  /** @type {ASTNode['parentNode']} */
  parentNode = null

  /** @type {ASTNode['isChained']} */
  isChained = false

  /** @type {string} content */
  content = ''

  /** @type {boolean} */
  encode = false

  /**
   * @param {LiteralNode['content']} content
   * @param {LiteralNode['encode']} encode
   * @param {LiteralNode['parentNode']} parentNode
   * @param {LiteralNode['children']} children
   */
  constructor(content, encode, parentNode = null, children = []) {
    this.parentNode = parentNode
    this.children = mapChildrenToParentNode(children, this)
    this.content = content
    this.encode = encode
    this.isChained = false
  }

  /** @returns {LiteralNode} */
  clone() {
    return new LiteralNode(
      this.content,
      this.encode,
      this.parentNode,
      this.children
    ).chain(this.isChained)
  }

  /** @returns {string} */
  toString(truncateChildren = false, depth = 0) {
    return nodeFormatter(
      truncateChildren,
      depth,
      'LiteralNode',
      { content: this.content, encode: this.encode, isChained: this.isChained },
      this.children
    )
  }

  /**
   * @param {boolean} [isChained=true]
   * @returns {LiteralNode}
   */
  chain(isChained = true) {
    this.isChained = isChained
    return this
  }
}
/** @param {ASTNode[]} children
 * @param {ASTNode} parentNode
 * @returns {any[]}
 */
export function mapChildrenToParentNode(children, parentNode) {
  children.forEach(c => (c.parentNode = parentNode))
  return children
}

/** @typedef {{type: 'html' | 'css', key: string, values: any[]}} TagNodeProp */
