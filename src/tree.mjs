import { NoxHTMLASTError } from './ast.mjs'
import { RootNode } from './nodes.mjs'

/** @typedef {import("./nodes.mjs").ASTNode} ASTNode */

import logger from 'loglevel'
const log = logger.getLogger('Tree')
// log.setLevel('trace')

/** */
export class Tree {
  /** @type {RootNode | null} */
  ast = null
  /** @type {RootNode | null} */
  currentNode = null

  /** @type {ASTNode | null}  */
  lastInsertedNode = null
  constructor() {
    this.ast = new RootNode([])
    this.currentNode = this.ast
    this.lastInsertedNode = null
  }

  // Travel tree down, closer to root
  /** @returns {void} */
  climbDown() {
    if (this.currentNode === null || this.currentNode.parentNode === null) {
      throw new NoxHTMLASTError("Couldn't climb tree further down", -2)
    }
    let currentNode = this.currentNode
    // If climbing down, make sure we are not climbing to down on chained node
    if (currentNode.isChained) {
      log.debug(
        '{Tree.climDown()} currentNode is chained, finding parent node that is not chained'
      )
      while (currentNode.isChained) {
        if (currentNode.parentNode === null) {
          throw new Error(
            'isChained is true but parentNode is null, this should not happen'
          )
        }
        currentNode = currentNode.parentNode
      }
    }
    if (currentNode.parentNode === null) {
      throw new Error(
        'isChained is true but parentNode is null, this should not happen'
      )
    }
    currentNode = currentNode.parentNode
    log.debug(
      `{Tree.climbDown()} climbed down to ${currentNode.toString(true)}`
    )
    this.currentNode = currentNode
  }

  // Travel tree up
  /** @param {ASTNode} node
   * @returns {void}
   */
  climbToNode(node) {
    log.debug(`{Tree.climbToNode()} climb to ${node.toString(true)}`)
    this.currentNode = node
  }

  // Insert node at current position
  /** @param {ASTNode} node
   * @returns {any}
   */
  insertNode(node) {
    log.debug(`{Tree.insertNode()} Inserting node: ${node.toString()}`)
    let parentNode = this.currentNode
    // Chained nodes are getting added to the last inserted node
    if (node.isChained) {
      if (this.lastInsertedNode === null) {
        throw new Error('Error: Cannot chain node to nothing')
      }
      parentNode = this.lastInsertedNode
    }
    const nodeWithParent = node.clone()
    nodeWithParent.parentNode = parentNode
    if (parentNode !== null) {
      parentNode.children.push(nodeWithParent)
    }
    this.lastInsertedNode = nodeWithParent
    return nodeWithParent
  }
}
