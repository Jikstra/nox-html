/** @returns {{ file: string; line: number; column: number; }} */
export function getLineAndFileOfCaller() {
  let returnValue = { file: 'unknown', line: -1, column: -1 }
  // return returnValue
  const stack = new Error().stack
  if (!stack) {
    throw new Error('Could not resolve stack')
  }
  const lines = stack.split('\n')
  if (lines[3].indexOf('(') !== -1) {
    const line = lines[3].trim()
    const [file, line_number, column] = line
      .replace('file://', '')
      .split('(')[1]
      .split(':')
    returnValue = {
      file,
      line: Number.parseInt(line_number),
      column: Number.parseInt(column),
    }
  } else if (lines[7].indexOf('(') !== -1) {
    const [file, line, column] = lines[7].split('(')[1].split(':')
    returnValue = {
      file,
      line: Number.parseInt(line),
      column: Number.parseInt(column),
    }
  }
  return returnValue
}
/** @param {any[]} arr
 * @returns {any}
 */
export function arrayLast(arr) {
  if (arr.length === 0) {
    return null
  }
  return arr[arr.length - 1]
}
/**
 * @template D
 * @param {D} toClone
 * @returns {D}
 */
export function structuredClone(toClone) {
  return JSON.parse(JSON.stringify(toClone))
}
