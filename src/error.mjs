import { reflect } from 'nox-reflect'

/**
 * @param {string} data
 * @param {number} pos
 * @returns {ExtractedLine}
 */
export function extractLineAtPos(data, pos, offset_line_number = 0) {
  const extracted_line = {
    line_number: 1 + offset_line_number,
    column: 0,
    start_pos: 0,
    end_pos: data.length,
    text: '',
  }
  // Find beginning of line
  let i = 0
  while (i <= pos) {
    const char = data[i]
    if (char === '\n') {
      extracted_line.line_number = extracted_line.line_number + 1
      extracted_line.start_pos = i + 1
      extracted_line.column = 0
    } else {
      extracted_line.column = extracted_line.column + 1
    }
    i++
  }
  // Find end of line
  while (i < data.length) {
    const char = data[i]
    if (char === '\n') {
      extracted_line.end_pos = i
      break
    }
    i++
  }
  // If we reach EOF before newline, last character is line end
  if (extracted_line.end_pos === -1) {
    extracted_line.end_pos = i
  }
  extracted_line.text = data.substring(
    extracted_line.start_pos,
    extracted_line.end_pos
  )
  return extracted_line
}

/**
 * @param {string} data
 * @param {number} pos
 * @returns {ExtractedLines}
 */
export function extractLines(data, pos, offset_main_line = 0) {
  const main = extractLineAtPos(data, pos, offset_main_line)
  let before = null
  if (main.start_pos - 2 > 0) {
    before = extractLineAtPos(data, main.start_pos - 2, offset_main_line)
    // As we don't know the real position, set column to 1
    before.column = 1
  }
  let after = null
  if (main.end_pos + 2 < data.length - 1) {
    after = extractLineAtPos(data, main.end_pos + 2, offset_main_line)
    // As we don't know the real position, set column to 1
    after.column = 1
  }
  return { before, main, after }
}

/**
 * @param {string} file
 * @param {string} data
 * @param {number} pos
 * @param {string} message
 * @param {string} message_human
 * @returns {string}
 */
export function formatLineWithError(
  file,
  data,
  pos,
  message,
  message_human,
  offset_main_line = 0
) {
  const extracted_lines = extractLines(data, pos, offset_main_line)
  const SPACER = ' '.repeat(4)
  const main_line_number_length = String(
    extracted_lines.main.line_number
  ).length
  let result = ''
  result +=
    SPACER +
    '--> ' +
    file +
    ':' +
    extracted_lines.main.line_number +
    ':' +
    extracted_lines.main.column +
    '\n'
  if (extracted_lines.before) {
    result +=
      SPACER +
      extracted_lines.before.line_number +
      ' | ' +
      extracted_lines.before.text +
      '\n'
  }
  result +=
    SPACER +
    extracted_lines.main.line_number +
    ' | ' +
    extracted_lines.main.text +
    '\n'
  const column_repeat = ' '.repeat(Math.max(extracted_lines.main.column - 1, 0))
  result +=
    SPACER +
    ' '.repeat(main_line_number_length) +
    ' | ' +
    column_repeat +
    '^ ' +
    (message_human !== '' ? message_human : message) +
    '\n'
  if (extracted_lines.after) {
    result +=
      SPACER +
      extracted_lines.after.line_number +
      ' ' +
      '|' +
      ' ' +
      extracted_lines.after.text +
      '\n'
  }
  return result
}

/** @enum {string} */
export const NoxHTMLErrorType = {
  Tokenizer: 'Tokenizer',
  AST: 'AST',
  toHTML: 'toHTML',
}

/** @extends Error */
export class NoxHTMLError extends Error {
  /** @type {number} */
  position = -1

  /** @type {string} */
  type = ''

  /** @type {boolean} */
  should_beautify_stack = false

  /**
   * @param {NoxHTMLError['type']} type
   * @param {NoxHTMLError['message']} message
   * @param {NoxHTMLError['position']} position
   * @param {NoxHTMLError['should_beautify_stack']} [beautify_stack=true]
   */
  constructor(type, message, position, beautify_stack = true) {
    super(`Error at position ${position}: ${message}`)
    this.name = 'NoxHTMLError'
    this.type = type
    this.position = position
    this.should_beautify_stack = beautify_stack
  }

  /**
   * @param {any} constructor_opt
   * @param {number} call_stack_offset
   * @param {TemplateStringsArray} strings
   * @param {any[]} values
   * @returns {void}
   */
  // eslint-disable-next-line no-unused-vars
  beautify(constructor_opt, call_stack_offset, strings, ...values) {
    const reflect_object = reflect(call_stack_offset)
    const file = (reflect_object && reflect_object.file) || '??'
    const offset_line = (reflect_object && reflect_object.lineNumber) || 0
    const joined_string = strings.join(' ')
    const message =
      '\n' +
      formatLineWithError(
        file,
        joined_string,
        this.position,
        this.message,
        '',
        offset_line
      )
    const beautified_err = new Error(message)
    Error.captureStackTrace(beautified_err, constructor_opt)
    throw beautified_err
  }
}

/**
 * @typedef {{
 *   before: ExtractedLine | null
 *   main: ExtractedLine
 *   after: ExtractedLine | null
 * }} ExtractedLines
 */

/** @typedef {Object} ExtractLines
 * @property {number} line
 * @property {number} column
 * @property {string} lineText
 * @property {string|null} lineBeforeText
 * @property {string|null} lineAfterText
 */

/** @typedef {Object} ExtractedLine
 * @property {number} line_number
 * @property {number} column
 * @property {number} start_pos
 * @property {number} end_pos
 * @property {string} text
 */
