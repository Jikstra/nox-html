'use strict'

import logger from 'loglevel'
import { NoxHTMLError, NoxHTMLErrorType } from './error.mjs'
import { LiteralNode, TagNode } from './nodes.mjs'
import { Tree } from './tree.mjs'
import { getLineAndFileOfCaller } from './utils.mjs'

const log = logger.getLogger('Tokenizer')
// log.setLevel('trace')


function unimplemented() {
  throw new Error('unimplemented')
}

/** @extends NoxHTMLError */
export class NoxHTMLASTError extends NoxHTMLError {
  constructor(/** @type{string} */ message, /** @type{number} */ position) {
    super(NoxHTMLErrorType.AST, message, position)
  }
}

/** @enum {string} */
const ASTState = {
  INIT: 'INIT',
  TAG: 'TAG',
  CLASS_ID_TAG_OPERATOR: 'CLASS_ID_TAG_OPERATOR',
  TAG_SEPERATOR_OPEN: 'TAG_SEPERATOR_OPEN',
  TAG_PROPS_IDENTIFIER: 'TAG_PROPS_IDENTIFIER',
  TAG_PROPS_EQUALS: 'TAG_PROPS_EQUALS',
}

/**
 * @param {ASTState} currentState
 * @param {import("./tokenizer.mjs").Token} token
 * @returns {import("./ast.mjs").NoxHTMLASTError}
 */
function errorInvalidASTState(currentState, token) {
  return new NoxHTMLASTError(
    `Being in ${currentState} state and received an invalid token of type ${
      token.type
    }.\n\tRaw: ${JSON.stringify(token)}`,
    token.position
  )
}
/** */
export class Collected {
  /** @type {string} */
  tag = ''

  /** @type {'class' | 'id' | ''} */
  class_id_tag = ''

  /** @type {string} */
  current_identifier = ''

  /** @type {'html'|'css'} */
  current_identifier_type = 'html'

  /** @type {TagNode['props']} */
  props = []

  /** @type {null|function} */
  mixin = null

  /** @type {boolean} */
  isChained = false

  constructor() {
    this.reset()
  }

  /** @returns {void} */
  reset() {
    log.debug('reset collected')
    this.tag = ''
    this.class_id_tag = ''
    this.current_identifier = ''
    this.props = []
    this.isChained = false
    this.mixin = null
  }

  /**
   * @param {import("./tokenizer.mjs").TemplateValueToken['value']} value
   * @returns {string[] | null | undefined}
   */
  stringifyValue(value) {
    // @ts-ignore
    if (typeof value === 'boolean' || value === null || typeof value === 'undefined') {
      return [String(value)]
    } else if (typeof value === 'string') {
      return [value]
    } else if (typeof value === 'function') {
      return this.stringifyValue(value())
    } else if (Array.isArray(value)) {
      const filtered_values = /** @type {string[]} */ (
        /** @type {unknown} */
        value.filter(element => typeof element === 'string')
      )
      return filtered_values
    } else if (typeof value === 'object') {
      return Object.entries(value)
        .filter(([_key, value]) => {
          // @ts-ignore
          return value == true
        })
        .map(([key, _value]) => key)
    }
    unimplemented()
  }


  /**
   * @param {import("./tokenizer.mjs").TemplateValueToken['value']} value
   * @returns {void}
   */
  addCurrentProp(value) {
    this._addProp(this.current_identifier, value)
    this.resetCurrentIdentifier()
  }

  resetCurrentIdentifier() {
    this.current_identifier = ''
    this.current_identifier_type = 'html'
  }

  /**
   * @param {string} key
   * @param {import("./tokenizer.mjs").TemplateValueToken['value']} value
   * @returns {void}
   */
  _addProp(key, value) {
    log.debug(`{Collected.addProp} key=${key} value=${JSON.stringify(value)}`)
    for (const prop of this.props) {
      if (prop.key != key) {
        continue
      }
      prop.values.push(value)
      return
    }
    this.props.push({
      type: this.current_identifier_type,
      key,
      values: [value],
    })
  }

  /** @returns {any} */
  toTagNode() {
    let node = new TagNode(this.tag, this.props, null, [])
      .chain(this.isChained)
      .set_mixin(this.mixin)

    return node
  }

  /** @returns {string} */
  toString() {
    return `Collected {
            tag: ${JSON.stringify(this.tag)},
             class_id_tag: ${JSON.stringify(this.class_id_tag)},
             current_identifier: ${JSON.stringify(this.current_identifier)},
             current_identifier_type: ${JSON.stringify(
               this.current_identifier_type
             )},
             props: ${JSON.stringify(this.props)}
             isChained: ${this.isChained}
             mixin: ${this.mixin}
            }`
  }
}
/** */
export class ASTify {
  /** @type {Tree} */
  tree

  /** @type {Collected} */
  collected

  /** @type {ASTState} */
  state

  /** @type {import("./tokenizer.mjs").Token[]} */
  tokens = []

  /** @type {number} */
  i

  /**
   * @param {import("./tokenizer.mjs").Token[]} tokens
   */
  constructor(tokens) {
    log.debug('toAST')
    // Go away from root
    this.tree = new Tree()
    this.state = ASTState.INIT
    this.collected = new Collected()
    this.tokens = tokens
    this.i = 0
  }

  /** @returns {void} */
  next() {
    if (this.i < this.tokens.length) {
      const token = this.tokens[this.i]
      processToken(
        token,
        this.state,
        this.setState.bind(this),
        this.collected,
        this.tree
      )
      this.i++
    } else if (this.i === this.tokens.length) {
      logStateAndBranch(this.state, 'CLEAN UP')
      // Clean up left overs
      // @ts-ignore
      if (this.state === ASTState.TAG) {
        this.tree.insertNode(this.collected.toTagNode())
      } else if (this.state !== ASTState.INIT) {
        const position = this.tokens[this.tokens.length - 1].position
        throw new NoxHTMLASTError(
          `${this.state} is an invalid state at the EOF`,
          position
        )
      }
      this.i++
    }
  }

  /** @returns {boolean} */
  done() {
    return this.i > this.tokens.length
  }

  /** @param {ASTState} newState
   * @returns {void}
   */
  setState(newState) {
    log.debug(
      `{ASTState.${this.state}:${
        getLineAndFileOfCaller().line
      }} switching to ASTState.${newState}`
    )
    this.state = newState
  }
}

/** @typedef {import("./nodes.mjs").RootNode} RootNode */
/** @typedef {RootNode|null} AST  */

/**
 * @param {import("./tokenizer.mjs").Token[]} tokens
 * @returns {AST}
 */
export function toAST(tokens) {
  const astifyer = new ASTify(tokens)
  while (!astifyer.done()) {
    astifyer.next()
  }
  return astifyer.tree.ast
}

/**
 * @param {ASTState} state
 * @param {string} branch
 * @returns {void}
 */
export function logStateAndBranch(state, branch) {
  log.debug(
    `{ASTState.${state}:${getLineAndFileOfCaller().line}} Branch: ${branch}`
  )
}

/** @param {ASTState} state
 * @param {Collected} collected
 * @param {import("./tokenizer.mjs").Token} token
 * @returns {void}
 */
export function logState(state, collected, token) {
  log.debug(
    `\n{ASTState.${state}:${getLineAndFileOfCaller().line}} ${token.toString()}`
  )
  log.debug(
    `{ASTState.${state}:${
      getLineAndFileOfCaller().line
    }} ${collected.toString()}`
  )
}

/**
 * @param {Tree} tree
 * @returns {void}
 */
export function logTree(tree) {
  log.debug(
    `{Tree.lastInsertedNode:${
      getLineAndFileOfCaller().line
    }} ${tree.lastInsertedNode?.toString(true)}`
  )
  log.debug(
    `{Tree.currentNode:${
      getLineAndFileOfCaller().line
    }} ${tree.currentNode?.toString(true)}`
  )
}
// Helper function that creates tag node from collected, inserts it into the tree
// and resets collected
/** @param {Tree} tree
 * @param {Collected} collected
 * @returns {any}
 */
export function insertCollectedTagNodeAndReset(tree, collected) {
  const newTagNode = collected.toTagNode()
  collected.reset()
  return tree.insertNode(newTagNode)
}
/** @param {import("./tokenizer.mjs").TemplateValueToken['value']} template_value
 * @returns {any}
 */
export function templateValueToNode(template_value) {}
/**
 * @param {import("./tokenizer.mjs").Token} token
 * @param {ASTState} state
 * @param {(newState: ASTState) => void} setState
 * @param {Collected} collected
 * @param {Tree} tree
 * @returns {void}
 */
export function processToken(token, state, setState, collected, tree) {
  logState(state, collected, token)
  logTree(tree)
  if (state === ASTState.INIT) {
    if (token.type === 'identifier') {
      logStateAndBranch(state, "token.type === 'identifier'")
      collected.tag = token.identifier
      setState(ASTState.TAG)
    } else if (token.type === 'newline') {
      // Ignore newline tokens here and stay in init state
    } else if (token.type === 'seperator' && token.seperator === '{') {
      logStateAndBranch(
        state,
        "token.type === 'seperator' && token.seperator === '{'"
      )
      if (!tree.currentNode) {
        throw new Error('tree.currentNode is null which should not happen')
      }
      if (tree.currentNode.children.length > 0) {
        // Travel tree up to last children node
        const newCurrentNode =
          tree.currentNode.children[tree.currentNode.children.length - 1]
        tree.climbToNode(newCurrentNode)
      }
    } else if (token.type === 'seperator' && token.seperator === '}') {
      logStateAndBranch(
        state,
        "token.type === 'seperator' && token.seperator === '}'"
      )
      if (!tree.currentNode) {
        throw new Error('tree.currentNode is null which should not happen')
      }
      if (tree.currentNode.type === 'root') {
        throw new NoxHTMLASTError(
          'Trying to close a nesting but we are already at the root node',
          token.position
        )
      }
      tree.climbDown()
      setState(ASTState.INIT)
    } else if (token.type === 'literal') {
      logStateAndBranch(state, "token.type === 'literal'")
      const literalNode = new LiteralNode(token.literal, true, null, []).chain(
        collected.isChained
      )
      collected.reset()
      tree.insertNode(literalNode)
    } else if (token.type === 'operator' && token.operator === '.') {
      logStateAndBranch(
        state,
        "token.type === 'operator' && token.operator === '.'"
      )
      collected.tag = 'div'
      collected.class_id_tag = 'class'
      setState(ASTState.CLASS_ID_TAG_OPERATOR)
    } else if (token.type === 'operator' && token.operator === '#') {
      logStateAndBranch(
        state,
        "token.type === 'operator' && token.operator === '#'"
      )
      collected.tag = 'div'
      collected.class_id_tag = 'id'
      setState(ASTState.CLASS_ID_TAG_OPERATOR)
    } else if (token.type === 'operator' && token.operator === ':') {
      logStateAndBranch(
        state,
        "token.type === 'operator' && token.operator === ':'"
      )
      collected.isChained = true
    } else if (token.type === 'template_value') {
      logStateAndBranch(state, "token.type === 'template_value'")
      if (typeof token.value === 'string') {
        let node = new LiteralNode(token.value, false, null, [])
        node.chain(collected.isChained)
        collected.reset()
        tree.insertNode(node)
      } else if (typeof token.value === 'function') {
        collected.mixin = token.value
        setState(ASTState.TAG)
      } else if (Array.isArray(token.value)) {
        for (let element of token.value) {
          if (typeof element === 'string') {
            let node = new LiteralNode(element, false, null, [])
            node.chain(collected.isChained)
            collected.reset()
            tree.insertNode(node)            
          } else {
            unimplemented()
          }
        }
      } else if (typeof token.value === 'object') {
        unimplemented()
      } else if (token.value === null || typeof token.value === 'undefined' || typeof token.value === 'boolean' || typeof token.value === 'number') {
        let node = new LiteralNode(String(token.value), true)  
        node.chain(collected.isChained)
        collected.reset()
        tree.insertNode(node)
      } else {
        throw new Error(
          `Unimplemented typeof token.value=${typeof token.value}`
        )
      }
    } else {
      throw errorInvalidASTState(state, token)
    }
  } else if (state === ASTState.TAG) {
    if (token.type === 'seperator' && token.seperator === '{') {
      logStateAndBranch(
        state,
        "token.type === 'seperator' && token.seperator === '{'"
      )
      tree.climbToNode(insertCollectedTagNodeAndReset(tree, collected))
      setState(ASTState.INIT)
    } else if (token.type === 'seperator' && token.seperator === '(') {
      logStateAndBranch(
        state,
        "token.type === 'seperator' && token.seperator === '('"
      )
      setState(ASTState.TAG_SEPERATOR_OPEN)
    } else if (token.type === 'seperator' && token.seperator === '}') {
      logStateAndBranch(
        state,
        "token.type === 'seperator' && token.seperator === '}'"
      )
      insertCollectedTagNodeAndReset(tree, collected)
      tree.climbDown()
      setState(ASTState.INIT)
    } else if (token.type === 'operator' && token.operator === '.') {
      logStateAndBranch(
        state,
        "token.type === 'operator' && token.operator === '.'"
      )
      collected.class_id_tag = 'class'
      setState(ASTState.CLASS_ID_TAG_OPERATOR)
    } else if (token.type === 'operator' && token.operator === '#') {
      logStateAndBranch(
        state,
        "token.type === 'operator' && token.operator === '#'"
      )
      collected.class_id_tag = 'id'
      setState(ASTState.CLASS_ID_TAG_OPERATOR)
    } else if (token.type === 'operator' && token.operator === ':') {
      logStateAndBranch(
        state,
        "token.type === 'operator' && token.operator === ':'"
      )
      insertCollectedTagNodeAndReset(tree, collected)
      setState(ASTState.INIT)
      collected.isChained = true
    } else if (token.type === 'template_value') {
      logStateAndBranch(state, "token.type === 'template_value'")
      collected.tag += token.value
    } else if (token.type === 'identifier') {
      logStateAndBranch(state, "token.type === 'identifier'")
      insertCollectedTagNodeAndReset(tree, collected)
      collected.tag = token.identifier
      setState(ASTState.TAG)
    } else if (token.type === 'literal') {
      logStateAndBranch(state, "token.type === 'literal'")
      insertCollectedTagNodeAndReset(tree, collected)
      const literalNode = new LiteralNode(token.literal, true, null, [])
      tree.insertNode(literalNode)
      setState(ASTState.INIT)
    } else if (token.type === 'newline') {
      logStateAndBranch(state, "token.type === 'newline'")
      insertCollectedTagNodeAndReset(tree, collected)
      setState(ASTState.INIT)
    } else {
      throw errorInvalidASTState(state, token)
    }
  /* state === TAG_SEPERATOR_OPEN */
  } else if (state === ASTState.TAG_SEPERATOR_OPEN) {
    if (token.type === 'identifier') {
      logStateAndBranch(state, "token.type === 'identifier'")
      collected.current_identifier = token.identifier
      setState(ASTState.TAG_PROPS_IDENTIFIER)
    } else if (token.type === 'seperator' && token.seperator === ',') {
      logStateAndBranch(
        state,
        "token.type === 'seperator' && token.seperator === ','"
      )
    } else if (token.type === 'seperator' && token.seperator === ')') {
      logStateAndBranch(
        state,
        "token.type === 'seperator' && token.seperator === ')'"
      )
      const tagNode = collected.toTagNode()

      tree.insertNode(tagNode)
      collected.reset()
      setState(ASTState.INIT)
    } else if (token.type === 'seperator' && token.seperator === '}') {
      logStateAndBranch(
        state,
        "token.type === 'seperator' && token.seperator === '}'"
      )
      if (!tree.currentNode) {
        throw new Error('tree.currentNode is null which should not happen')
      }
      if (tree.currentNode.type === 'root') {
        throw new NoxHTMLASTError(
          'Trying to close a nesting but we are already at the root node',
          token.position
        )
      }
      tree.climbDown()
      setState(ASTState.INIT)
    } else if (token.type === 'literal') {
      logStateAndBranch(state, "token.type === 'literal'")
      collected.current_identifier = token.literal
      setState(ASTState.TAG_PROPS_IDENTIFIER)
    } else if (
      token.type === 'template_value' &&
      typeof token.value === 'string'
    ) {
      logStateAndBranch(
        state,
        "token.type === 'template_value' && typeof token.value === 'string'"
      )
      collected.current_identifier = token.value
      setState(ASTState.TAG_PROPS_IDENTIFIER)
    } else if (token.type === 'operator' && token.operator === '%') {
      collected.current_identifier_type = 'css'
    } else if (token.type === 'newline') {
      // Ignore newlines
    } else {
      throw errorInvalidASTState(state, token)
    }
    /* state === TAG_PROPS_IDENTIFIER */
  } else if (state === ASTState.TAG_PROPS_IDENTIFIER) {
    if (token.type === 'operator' && token.operator === '=') {
      logStateAndBranch(
        state,
        "token.type === 'operator' && token.operator === '='"
      )
      setState(ASTState.TAG_PROPS_EQUALS)
    } else if (token.type === 'seperator' && token.seperator === ')') {
      logStateAndBranch(
        state,
        "token.type === 'seperator' && token.seperator === ')'"
      )
      collected.addCurrentProp(true)
      tree.insertNode(collected.toTagNode())
      collected.reset()
      setState(ASTState.INIT)
    } else if (token.type === 'identifier') {
      if (collected.current_identifier) {
        collected.addCurrentProp(true)
      }
      collected.current_identifier = token.identifier
    } else if (token.type === 'literal') {
      if (collected.current_identifier) {
        collected.addCurrentProp(true)
      }
      collected.current_identifier = `"${token.literal}"`
    } else {
      throw errorInvalidASTState(state, token)
    }
  } else if (state === ASTState.TAG_PROPS_EQUALS) {
    if (token.type === 'literal') {
      logStateAndBranch(state, "token.type === 'literal'")
      collected.addCurrentProp(token.literal)
      setState(ASTState.TAG_SEPERATOR_OPEN)
    } else if (token.type === 'template_value') {
      logStateAndBranch(state, "token.type === 'template_value'")
      collected.addCurrentProp(token.value)
      setState(ASTState.TAG_SEPERATOR_OPEN)
    } else {
      throw errorInvalidASTState(state, token)
    }
  } else if (state === ASTState.CLASS_ID_TAG_OPERATOR) {
    if (token.type === 'identifier') {
      logStateAndBranch(state, "token.type === 'identifier'")
      collected._addProp(collected.class_id_tag, token.identifier)
      setState(ASTState.TAG)
    } else {
      throw errorInvalidASTState(state, token)
    }
  } else {
    throw errorInvalidASTState(state, token)
  }
}
