import logger from 'loglevel'
import { NoxHTMLError, NoxHTMLErrorType } from './error.mjs'
import { getLineAndFileOfCaller } from './utils.mjs'
const log = logger.getLogger('Tokenizer')
// log.setLevel('trace')

/**
 * @param {string} char
 * @param {number} pos
 * @returns {TemplateChar}
 */
export function createTemplateChar(char, pos) {
  return {
    type: 'char',
    char,
    pos,
  }
}
/**
 * @param {any} value
 * @param {number} pos
 * @returns {TemplateValue}
 */
export function createTemplateValue(value, pos) {
  return {
    type: 'value',
    value,
    pos,
  }
}
/**
 * @param {TemplateStringsArray} strings
 * @param {any[]} values
 * @returns {Generator<TemplateChar | TemplateValue, void, unknown>}
 */
export function* TemplateIterator(strings, ...values) {
  let pos = 0
  for (let i = 0; i < strings.length; i++) {
    const string = strings[i]
    for (let j = 0; j < string.length; j++) {
      yield createTemplateChar(string[j], pos)
      pos++
    }
    if (i < values.length) {
      yield createTemplateValue(values[i], pos)
      pos++
    }
  }
}
/**
 * @param {string} tokenName
 * @param {number} position
 * @param {{[key: string]: string}} props
 * @returns {string}
 */
export function tokenToString(tokenName, position, props) {
  const propsString = Object.keys(props)
    .map(p => `${p}=${props[p]}`)
    .join(', ')
  return `${tokenName} { position: ${position}, ${propsString} }`
}
/** @implements BaseToken */
export class IdentifierToken {
  /** @type {'identifier'} */
  type = 'identifier'

  /** @type {number} */
  position = -1

  /** @type {string} */
  identifier = ''

  /**
   * @param {IdentifierToken['identifier']} identifier
   * @param {IdentifierToken['position']} position
   */
  constructor(identifier, position) {
    this.type = 'identifier'
    this.position = position
    this.identifier = identifier
  }

  /** @returns {string} */
  toString() {
    return tokenToString('IdentifierToken', this.position, {
      identifier: this.identifier,
    })
  }
}

/** @implements BaseToken */
export class SeperatorToken {
  /** @type {'seperator'} */
  type = 'seperator'

  /** @type {BaseToken['position']} */
  position = -1

  /** @type {string} */
  seperator = ''

  /**
   * @param {SeperatorToken['seperator']} seperator
   * @param {SeperatorToken['position']} position
   */
  constructor(seperator, position) {
    this.type = 'seperator'
    this.position = position
    this.seperator = seperator
  }

  /** @returns {string} */
  toString() {
    return tokenToString('SeperatorToken', this.position, {
      seperator: this.seperator,
    })
  }
}

/** @implements BaseToken */
export class OperatorToken {
  /** @type {'operator'} */
  type = 'operator'

  /** @type {BaseToken['position']} */
  position = -1

  /** @type {string} */
  operator = ''

  /**
   * @param {OperatorToken['operator']} operator
   * @param {OperatorToken['position']} position
   */
  constructor(operator, position) {
    this.type = 'operator'
    this.position = position
    this.operator = operator
  }

  /** @returns {string} */
  toString() {
    return tokenToString('OperatorToken', this.position, {
      operator: this.operator,
    })
  }
}

/** @implements BaseToken */
export class LiteralToken {
  /** @type {'literal'} */
  type = 'literal'

  /** @type {BaseToken['position']} */
  position = -1

  /** @type {string}  */
  literal = ''

  /**
   * @param {LiteralToken['literal']} literal
   * @param {LiteralToken['position']} position
   */
  constructor(literal, position) {
    this.type = 'literal'
    this.position = position
    this.literal = literal
  }

  /** @returns {string} */
  toString() {
    return tokenToString('LiteralToken', this.position, {
      literal: this.literal,
    })
  }
}

/** @implements BaseToken */
export class TemplateValueToken {
  /** @type {'template_value'} */
  type = 'template_value'

  /** @type {BaseToken['position']} */
  position = -1

  /** @type {string | boolean | Function | {[key: string]: boolean|null|undefined} | Array<string| boolean| null| undefined> | null | undefined} */
  value = ''

  /**
   * @param {TemplateValueToken['value']} value
   * @param {TemplateValueToken['position']} position
   */
  constructor(value, position) {
    this.type = 'template_value'
    this.position = position
    this.value = value
  }

  /** @returns {string} */
  toString() {
    return tokenToString('TemplateValueToken', this.position, {
      value: JSON.stringify(this.value),
    })
  }
}

/** @implements BaseToken */
export class NewlineToken {
  /** @type {'newline'} */
  type = 'newline'

  /** @type {BaseToken['position']} */
  position = -1

  /**
   * @param {TemplateValueToken['position']} position
   */
  constructor(position) {
    this.type = 'newline'
    this.position = position
  }

  /** @returns {string} */
  toString() {
    return tokenToString('NewlineToken', this.position, {})
  }
}


/** @extends NoxHTMLError */
export class NoxHTMLTokenizerError extends NoxHTMLError {
  /** @type {string} */
  message_human = ''

  /**
   * @param {NoxHTMLError['message']} message
   * @param {NoxHTMLTokenizerError['message_human']} message_human
   * @param {NoxHTMLError['position']} position
   */
  constructor(message, message_human, position) {
    super(NoxHTMLErrorType.Tokenizer, message, position)
    this.message_human = message_human
  }
}

/** @enum {string} */
export const TokenIteratorStates = {
  INIT: 'INIT',
  COULD_BE_IDENTIFIER: 'COULD_BE_IDENTIFIER',
  COULD_BE_LITERAL: 'COULD_BE_LITERAL',
  COULD_BE_PIPE_LITERAL: 'COULD_BE_PIPE_LITERAL',
  COULD_BE_PIPE_LITERAL_CHARS: 'COULD_BE_PIPE_LITERAL_CHARS',
  COULD_BE_PIPE_LITERAL_NEWLINE: 'COULD_BE_PIPE_LITERAL_NEWLINE',
}

const IDENTIFIER_CHARS = [
  'a',
  'b',
  'c',
  'd',
  'e',
  'f',
  'g',
  'h',
  'i',
  'j',
  'k',
  'l',
  'm',
  'n',
  'o',
  'p',
  'q',
  'r',
  's',
  't',
  'u',
  'v',
  'w',
  'x',
  'y',
  'z',
  'A',
  'B',
  'C',
  'D',
  'E',
  'F',
  'G',
  'H',
  'I',
  'J',
  'K',
  'L',
  'M',
  'N',
  'O',
  'P',
  'Q',
  'R',
  'S',
  'T',
  'U',
  'V',
  'W',
  'X',
  'Y',
  'Z',
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  '!',
  '-',
  '_',
]
const SEPERATOR_CHARS = ['(', ')', '{', '}', ',']
const OPERATOR_CHARS = ['=', '.', '#', ':', '%']
const LITERAL_CHARS = ['"', "'"]
const PIPE_LITERAL_CHARS = ['|']
const NEWLINE_CHAR = '\n'
const IGNORE_CHARS = [' ', '\t']

/**
 * @param {string[]} arr
 * @param {string} char
 * @return {boolean}
 */
function isValidChar(arr, char) {
  return arr.indexOf(char) !== -1
}

/**
 * @param {TokenIteratorStates} currentState
 * @param {string} char
 * @param {number} position
 * @returns {NoxHTMLTokenizerError}
 */
function errorInvalidTokenizerState(
  currentState,
  char,
  position,
  human_message = ''
) {
  return new NoxHTMLTokenizerError(
    `Tokenizer: Being in ${currentState} state and received this invalid char \`${char}\``,
    human_message,
    position
  )
}
/**
 * @param {TemplateStringsArray} strings
 * @param {any[]} values
 * @returns {Token[]}
 */
export function Tokenizer(strings, ...values) {
  const templateIterator = TemplateIterator(strings, ...values)
  const tokens = []

  /** @type {TokenIteratorStates} */
  let state = TokenIteratorStates.INIT
  const resetCollected = () => {
    return {
      collected: '',
      collected_position: -1,
      encode: true,
      pipe_indent: 0,
    }
  }
  let collected = resetCollected()
  const setState = (/** @type {TokenIteratorStates} */ newState) => {
    log.debug(
      `switching to ${newState} in line ${getLineAndFileOfCaller().line}`
    )
    state = newState
  }
  for (const templateElement of templateIterator) {
    const position = templateElement.pos
    log.debug(
      `state: ${state} elem: ${JSON.stringify(
        templateElement
      )} collected: ${JSON.stringify(collected)}`
    )
    if (templateElement.type === 'char') {
      const char = templateElement.char
      if (state == TokenIteratorStates.INIT) {
        if (isValidChar(IDENTIFIER_CHARS, char)) {
          log.debug(
            'char is valid identifier char, switching to COULD_BE_IDENTIFIER state'
          )
          collected.collected = char
          collected.collected_position = position
          setState(TokenIteratorStates.COULD_BE_IDENTIFIER)
          continue
        } else if (isValidChar(SEPERATOR_CHARS, char)) {
          tokens.push(new SeperatorToken(char, position))
          continue
        } else if (isValidChar(OPERATOR_CHARS, char)) {
          tokens.push(new OperatorToken(char, position))
          continue
        } else if (isValidChar(LITERAL_CHARS, char)) {
          collected.collected = char
          collected.collected_position = position
          setState(TokenIteratorStates.COULD_BE_LITERAL)
          continue
        } else if (isValidChar(PIPE_LITERAL_CHARS, char)) {
          setState(TokenIteratorStates.COULD_BE_PIPE_LITERAL)
          collected.collected_position = position
          continue
        } else if (char == NEWLINE_CHAR) {
          tokens.push(new NewlineToken(position))
          continue
        } else if (isValidChar(IGNORE_CHARS, char)) {
          continue
        } else {
          throw errorInvalidTokenizerState(
            state,
            char,
            position,
            'Unexpected character'
          )
        }
      } else if (state === TokenIteratorStates.COULD_BE_IDENTIFIER) {
        if (isValidChar(IDENTIFIER_CHARS, char)) {
          collected.collected += char
          continue
        } else if (isValidChar(SEPERATOR_CHARS, char)) {
          tokens.push(
            new IdentifierToken(
              collected.collected,
              collected.collected_position
            )
          )
          log.debug(`pushed identifier token ${collected.collected_position}`)
          collected = resetCollected()
          tokens.push(new SeperatorToken(char, position))
          setState(TokenIteratorStates.INIT)
          continue
        } else if (isValidChar(OPERATOR_CHARS, char)) {
          tokens.push(
            new IdentifierToken(
              collected.collected,
              collected.collected_position
            )
          )
          collected = resetCollected()
          tokens.push(new OperatorToken(char, position))
          log.debug(`pushed operator token '${char}' at ${position}`)
          setState(TokenIteratorStates.INIT)
          continue
        } else if (isValidChar(IGNORE_CHARS, char)) {
          tokens.push(
            new IdentifierToken(
              collected.collected,
              collected.collected_position
            )
          )
          collected = resetCollected()
          setState(TokenIteratorStates.INIT)
          continue
        } else if (char === NEWLINE_CHAR) {
          tokens.push(
            new IdentifierToken(
              collected.collected,
              collected.collected_position
            )
          )
          collected = resetCollected()
          tokens.push(
            new NewlineToken(
              position
            )
          )
          setState(TokenIteratorStates.INIT)
        } else {
          throw errorInvalidTokenizerState(state, char, position)
        }
      } else if (state === TokenIteratorStates.COULD_BE_LITERAL) {
        const openLiteralChar = collected.collected[0]
        log.debug(
          char,
          openLiteralChar,
          char === openLiteralChar,
          collected.collected[collected.collected.length - 1] !== '\\'
        )
        if (
          char === openLiteralChar &&
          collected.collected[collected.collected.length - 1] !== '\\'
        ) {
          tokens.push(
            new LiteralToken(
              collected.collected.substr(1, collected.collected.length - 1),
              collected.collected_position
            )
          )
          collected = resetCollected()
          setState(TokenIteratorStates.INIT)
          continue
        } else {
          collected.collected += char
            continue
        }
      } else if (state === TokenIteratorStates.COULD_BE_PIPE_LITERAL) {
        collected.collected += char
        if (char === NEWLINE_CHAR) {
          setState(TokenIteratorStates.COULD_BE_PIPE_LITERAL_NEWLINE)
          continue
        }
        setState(TokenIteratorStates.COULD_BE_PIPE_LITERAL_CHARS)
        continue
      } else if (state === TokenIteratorStates.COULD_BE_PIPE_LITERAL_CHARS) {
        collected.collected += char
        if (char === NEWLINE_CHAR) {
          setState(TokenIteratorStates.COULD_BE_PIPE_LITERAL_NEWLINE)
          continue
        }
      } else if (state === TokenIteratorStates.COULD_BE_PIPE_LITERAL_NEWLINE) {
        if (isValidChar(IGNORE_CHARS, char)) {
          continue
        } else if (isValidChar(PIPE_LITERAL_CHARS, char)) {
          setState(TokenIteratorStates.COULD_BE_PIPE_LITERAL)
          continue
        } else {
          tokens.push(
            new LiteralToken(collected.collected, collected.collected_position)
          )
          collected = resetCollected()
          setState(TokenIteratorStates.INIT)
          continue
        }
      } else {
        throw errorInvalidTokenizerState(state, char, position)
      }
    } else if (templateElement.type === 'value') {
      // @ts-ignore
      if (state === TokenIteratorStates.COULD_BE_IDENTIFIER) {
        collected.collected += String(templateElement.value)
        continue
        // @ts-ignore
      } else if (state === TokenIteratorStates.COULD_BE_LITERAL) {
        collected.collected += String(templateElement.value)
        continue
      } else if (state === TokenIteratorStates.INIT) {
        log.debug('templateElement is value, pushing it to tokens')
        tokens.push(new TemplateValueToken(templateElement.value, position))
        continue
      }
    }
  }

  log.debug(
    `state (EOF): ${state} position: ${collected.collected_position} collected: ${JSON.stringify(collected)}`
  )

  if (collected.collected != '') {
    if (state === TokenIteratorStates.INIT) {
      throw new Error('Illegal state: After state machine in init state??')
    } else if (state === TokenIteratorStates.COULD_BE_IDENTIFIER) {
      tokens.push(
        new IdentifierToken(collected.collected, collected.collected_position)
      )
    } else if (state === TokenIteratorStates.COULD_BE_LITERAL) {
      throw new NoxHTMLTokenizerError(
        `Unclosed literal, opened at position ${collected.collected_position}`,
        'This literal never gets closed, did you miss a `"`?',
        collected.collected_position
      )
    } else if (state === TokenIteratorStates.COULD_BE_PIPE_LITERAL_CHARS) {
      tokens.push(
        new LiteralToken(collected.collected, collected.collected_position)
      )
    } else if (state === TokenIteratorStates.COULD_BE_PIPE_LITERAL_NEWLINE) {
      tokens.push(
        new LiteralToken(collected.collected, collected.collected_position)
      )
    } else {
      throw errorInvalidTokenizerState(state, 'EOF', -1)
    }
  }
  return tokens
}

/**
 * @typedef {{
 *   type: 'char',
 *   char: string,
 *   pos: number
 * }} TemplateChar
 */

/**
 * @typedef {{
 *   type: 'value',
 *   value: any,
 *   pos: number
 * }} TemplateValue
 */

/** @typedef {TemplateValue | TemplateChar} TemplateElement */
/** @typedef {IdentifierToken | SeperatorToken | OperatorToken | LiteralToken | TemplateValueToken | NewlineToken} Token */
/** @typedef {[number, number]} LineColumn */

/**
 * @typedef {{
 *   collected: string,
 *   collected_position: number,
 *   encode: boolean,
 *   pipe_indent: number
 * }} TokenizerCollected
 */

/**
 * @typedef {Object} BaseToken
 * @property {string} type
 * @property {number} position
 */
