import { encode } from 'html-entities'

/** @typedef {import("./ast.mjs").AST} AST */
/** @typedef {import("./nodes.mjs").TagNode} TagNode */
/** @typedef {import("./nodes.mjs").LiteralNode} LiteralNode */
/** @typedef {import("./nodes.mjs").ASTNode} ASTNode */

/** @param {String} str */
export const camelToKebabCase = str =>
  str.replace(/[A-Z]/g, letter => `-${letter.toLowerCase()}`)

/**
 * @param {ASTNode} node
 * @returns {string}
 */
export function nodeToHTML(node) {
  if (node.type === 'tag') {
    const tag_node = /** @type {TagNode} */ (/** @type {unknown} */ node)
    return tagNodeToHTML(tag_node)
  } else if (node.type === 'literal') {
    const literal_node = /** @type {LiteralNode} */ (
      /** @type {unknown} */ node
    )
    return literalNodeToHTML(literal_node)
  }
  throw new Error(`Unimplemented node type of: ${node.type}`)
}
const VOID_ELEMENTS = [
  'area',
  'base',
  'br',
  'col',
  'embed',
  'hr',
  'img',
  'input',
  'link',
  'meta',
  'param',
  'source',
  'track',
  'wbr',
]
/** @param {string} tagName
 * @returns {boolean}
 */
export function isVoidElement(tagName) {
  return VOID_ELEMENTS.indexOf(tagName.toLowerCase()) !== -1
}
/** @param {TagNode} node
 * @returns {string}
 */
export function tagNodeToHTML(node) {
  const hasChildren = node.children.length !== 0
  const isDOCTYPE = node.tag.toLowerCase() === '!doctype'
  const selfClose = isVoidElement(node.tag)
  if (selfClose && hasChildren) {
    throw new Error('Self closing tag with children is not permitted')
  }
  if (isDOCTYPE && hasChildren) {
    throw new Error('!DOCTYPE tag with children is not permitted')
  }
  if (node.mixin) {
    const children_str = node.children.map(nodeToHTML).join('')
    const { css_attributes, html_attributes, style_attribute } =
      concatAttributes(node.props)
    const style = [stringifyCSSAttributes(css_attributes), style_attribute].filter(s => s !== '').join(';')

    
    const html_attributes_object = {}
    Array.from(html_attributes)
      .map(([k, values]) => [k, values.join(' ')])
      .forEach(([k, v]) => {
        html_attributes_object[k] = v
      })
    
    let return_value = node.mixin({ children: children_str, style, ...html_attributes_object })
    return return_value
  }
  let html = openingTagToString(node.tag, node.props, selfClose)
  if (!selfClose && !isDOCTYPE) {
    html += node.children.map(nodeToHTML).join('')
    html += `</${node.tag}>`
  }
  return html
}
/** @param {LiteralNode} node
 * @returns {string}
 */
export function literalNodeToHTML(node) {
  if (node.encode === false) {
    return node.content
  }
  return encode(node.content)
}

/**
 * @param {TagNode['props']} props
 * @returns {{html_attributes: Map<String, String[]>, css_attributes: Map<String, String[]>, style_attribute: String}}
 */
function concatAttributes(props) {
  const css_attributes = new Map()
  const html_attributes = new Map()

  /**
   * @param {Map<String, String[]>} map
   * @param {String} key
   * @param {String[]} values
   */
  const extend_map = (map, key, values) => {
    if (map.has(key)) {
      map.set(key, [...(map.get(key) || []), ...values])
    } else {
      map.set(key, values)
    }
  }

  props.forEach(prop => {
    const { key, type, values } = prop

    if (type === 'css') {
      extend_map(css_attributes, key, values)
    } else if (type === 'html') {
      extend_map(html_attributes, key, values)
    }
  })

  let style_attribute = ''
  if (html_attributes.has('style')) {
    style_attribute = html_attributes.get('style').join(';').replaceAll(' ', '').replaceAll("\n", '')
    html_attributes.delete('style')
    
  }

  return { html_attributes, css_attributes, style_attribute }
}

/**
 * @param {Map<String, String[]>} html_attributes
 * @return {string}
 */
function stringifyHTMLAttributes(html_attributes) {
  return Array.from(html_attributes)
    .map(([k, v]) => {
      let value_str = v.join(' ')

      if (v.length === 1 && v[0] === true) {
        return k
      }
      return `${k}="${value_str}"`
    })
    .join(' ')
}

/**
 * @param {Map<String, String[]>} css_attributes
 * @return {string}
 */
function stringifyCSSAttributes(css_attributes) {
  return Array.from(css_attributes)
    .map(([k, v]) => `${camelToKebabCase(k)}:${v}`)
    .join(';')
}

/** @param {string} tag
 * @param {TagNode['props']} props
 * @param {boolean} selfClose
 * @returns {string}
 */
function openingTagToString(tag, props, selfClose = false) {
  let { html_attributes, css_attributes, style_attribute: styleString } = concatAttributes(props)

  let htmlAttributesString = stringifyHTMLAttributes(html_attributes)
  styleString = [stringifyCSSAttributes(css_attributes), styleString].filter(s => s !== '').join(';')

  return `<${tag}${styleString !== '' ? ' style="' + styleString + '"' : ''}${
    htmlAttributesString ? ' ' + htmlAttributesString : ''
  }${selfClose ? '/' : ''}>`
}
/**
 * @param {AST} ast
 * @returns {string}
 */
export function AST2HTML(ast) {
  if (ast === null) return ''

  return ast.children.map(nodeToHTML).join('')
}
